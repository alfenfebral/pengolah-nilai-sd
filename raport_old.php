<?php 
include 'include/koneksi.php';
include 'include/fungsi.php';

if(isset($_GET['hasil'])){
	$hasil = $_GET['hasil'];
	$sql = "SELECT * FROM `hasil_raport` INNER JOIN `mengajar` ON `mengajar`.`id_mengajar`=`hasil_raport`.`id_mengajar` INNER JOIN `siswa` ON `siswa`.`id_siswa`=`mengajar`.`id_siswa` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`mengajar`.`id_kelas` WHERE `hasil_raport`.`id_hasil_raport`='$hasil'";
    $query = mysql_query($sql);
    $row = mysql_fetch_assoc($query);

    // Foreach Jumlah Mata Pelajaran
    $sql = "SELECT * FROM `mapel`";
    $query = mysql_query($sql);
    $arr = array();
    $p=0;
    while ($row1 = mysql_fetch_array($query)) {
    	$arr[$p]["id_mapel"] = $row1['id_mapel'];
    	$arr[$p]["nama_mapel"] = $row1['nama_mapel'];
    	$arr[$p]["kkm"] = $row1['kkm'];
    	$arr[$p]["jenis"] = $row1['jenis_mapel'];;
    	$p++;
    }

    function myfunction($products, $field, $value)
	{
	   foreach($products as $key => $product)
	   {
	      if ( $product[$field] === $value )
	         return $key;
	   }
	   return false;
	}

    // Kalkulasi Nilai Ulangan Harian
    $id_mengajar = $row['id_mengajar'];
    $semester = $row['semester'];
    $thn_pel = $row['thn_pel'];
    // $arr_ul = array();
    // $p_ul=0;
    foreach ($arr as $arr_cek) {
    	$sql = "SELECT id_mapel, AVG(nilai) AS `nilai_rata` FROM `ulangan_harian` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`ulangan_harian`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
    	$query = mysql_query($sql);
   		$row_ul = mysql_fetch_assoc($query);
   		if(isset($row_ul['id_mapel']) && isset($row_ul['nilai_rata'])){
   			// $arr_ul[$p_ul]["id_mapel"] = $row_ul['id_mapel'];
   			// $arr_ul[$p_ul]["nilai_rata"] = $row_ul['nilai_rata'];

   			// masukkan nilai rata rata ul ke array arr
   			$key = myfunction($arr, 'id_mapel', $row_ul['id_mapel']);
   			$arr[$key]['nilai_rata_ul'] = $row_ul['nilai_rata'];
   		}
   		// $p_ul++;
    }
    // print_r($arr_ul);

    // Kalkulasi Tugas/PR
    // $arr_tgs = array();
    // $p_tgs=0;
    foreach ($arr as $arr_cek) {
    	$sql = "SELECT id_mapel, AVG(nilai) AS `nilai_rata` FROM `tugas` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`tugas`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
    	$query = mysql_query($sql);
   		$row_tgs = mysql_fetch_assoc($query);
   		if(isset($row_tgs['id_mapel']) && isset($row_tgs['nilai_rata'])){
   			// $arr_tgs[$p_tgs]["id_mapel"] = $row_tgs['id_mapel'];
   			// $arr_tgs[$p_tgs]["nilai_rata"] = $row_tgs['nilai_rata'];
   			$key = myfunction($arr, 'id_mapel', $row_tgs['id_mapel']);
   			$arr[$key]['nilai_rata_tugas'] = $row_tgs['nilai_rata'];
   		}
   		// $p_tgs++;
    }
    // print_r($arr_tgs);

    // Kalkulasi UTS
    // $arr_uts = array();
    // $p_uts=0;
    foreach ($arr as $arr_cek) {
    	$sql = "SELECT id_mapel,tulis,praktek FROM `uts` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`uts`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
    	$query = mysql_query($sql);
   		$row_uts = mysql_fetch_assoc($query);
   		if(isset($row_uts['id_mapel']) && isset($row_uts['tulis']) && isset($row_uts['praktek'])){
   			// $arr_uts[$p_uts]["id_mapel"] = $row_uts['id_mapel'];
   			// $arr_uts[$p_uts]["nilai_rata"] = ($row_uts['tulis']+$row_uts['praktek'])/2;
   			$key = myfunction($arr, 'id_mapel', $row_uts['id_mapel']);
   			$arr[$key]['nilai_rata_uts'] = ($row_uts['tulis']+$row_uts['praktek'])/2;
   		}
   		// $p_uts++;
    }
    // print_r($arr_uts);

    //Kalkulasi UAS
    // $arr_uas = array();
    // $p_uas=0;
    foreach ($arr as $arr_cek) {
    	$sql = "SELECT id_mapel,tulis,praktek FROM `uas` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`uas`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
    	$query = mysql_query($sql);
   		$row_uas = mysql_fetch_assoc($query);
   		if(isset($row_uas['id_mapel']) && isset($row_uas['tulis']) && isset($row_uas['praktek'])){
   			// $arr_uas[$p_uas]["id_mapel"] = $row_uas['id_mapel'];
   			// $arr_uas[$p_uas]["nilai_rata"] = ($row_uas['tulis']+$row_uas['praktek'])/2;
   			$key = myfunction($arr, 'id_mapel', $row_uas['id_mapel']);
   			$arr[$key]['nilai_rata_uas'] = ($row_uas['tulis']+$row_uas['praktek'])/2;
   		}
   		// $p_uas++;
    }
    // print_r($arr_uas);

    // foreach ($arr as $arr_t) {
    // 	if(in_array("mac", $arr)){
    // 		// masukkin ke array ar nilainya
    // 	}else{
    // 		// jika mapel tak ada isi value 0
    // 	}
    // }

	// echo array_search('Pendidikan Kewarganegaraan', array_keys($arr));

	// print_r($arr);

    $i=0;
	foreach ($arr as $rata) {
		if(!isset($rata['nilai_rata_ul']))
			$rata['nilai_rata_ul'] = 0;
		if(!isset($rata['nilai_rata_tugas']))
			$rata['nilai_rata_tugas'] = 0;
		if(!isset($rata['nilai_rata_uts']))
			$rata['nilai_rata_uts'] = 0;
		if(!isset($rata['nilai_rata_uas']))
			$rata['nilai_rata_uas'] = 0;
		$arr[$i]['nilai_hasil'] = ($rata['nilai_rata_ul'] + $rata['nilai_rata_tugas'] + $rata['nilai_rata_uts'] + $rata['nilai_rata_uas'])/4;
		
		$i++;
	}
	// print_r($arr);


}else{
	header("location: index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>
		Hasil Raport
	</title>
	<link rel="stylesheet" type="text/css" href="print-raport.css">
  <style type="text/css">
    body{
      padding:px;
      margin:0px;
    }
    .header {
      font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
      background: #2C625E;
      color: white;
      font-size: 30px;
      padding: 5px;
  }
  #container{
    margin-left: 5px;
    margin-right: 5px;
  }
  </style>
</head>
<body>
<div class="header"><a style="color: black; text-decoration: none;" href="<?php echo $_SERVER['HTTP_REFERER']; ?>"><<</a> HASIL RAPORT SDN CIPARIGI</div>
<div id="container">
<div class="biodata">
Nama Peserta Didik : <?= $row['nama_siswa']; ?> <br/>
Nomor Induk / NISN : 0000 / 0000<br/>
Nama Sekolah : SDN Ciparigi<br/>
Alamat Sekolah : Jl. Ciburial no.10 Rt 004/04 Bogor<br/>
</div>
<div class="biodata">
Kelas : <?= $row['nama_kelas']; ?><br/>
Semester : <?= romawi($row['semester']); ?><br/>
Tahun Pelajaran : <?= $row['thn_pel']; ?><br/>
</div>
<div class="clear"/>
<br/>
<table border="1" style="border-collapse: collapse;">
	<tbody>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Mata Pelajaran</th>
			<th rowspan="2" width="50px;">Kriteria Ketuntasan Minimal</th>
			<th colspan="2">Nilai</th>
		</tr>
		<tr>
			<th>Angka</th>
			<th>Huruf</th>
		</tr>
		<?php 
		$no = 0;
		?>
		<?php foreach ($arr as $arr_hsl): ?>
			<?php 
			$no++;
			?>
			<tr>
			<td class="text-center"><?= $no; ?></td>
			<td><?= $arr_hsl['nama_mapel']; ?></td>
			<td class="text-center"><?= $arr_hsl['kkm']; ?></td>
			<td class="text-center"><?= intval($arr_hsl['nilai_hasil']); ?></td>
			<td><?= huruf_awal(terbilang(intval($arr_hsl['nilai_hasil']))); ?></td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>
<br>
<table border="1" style="border-collapse: collapse;">
	<tbody>
		<tr>
			<th class="text-center">No.</th>
			<th class="text-center">Kepribadian</th>
			<th class="text-center">Nilai</th>
			<th class="text-center">Ketidakhadiran</th>
			<th class="text-center">Hari</th>
		</tr>
		<tr>
			<td>1</td>
			<td>Sikap</td>
			<td><?= $row['sikap']; ?></td>
			<td>Izin</td>
			<td><?= $row['izin']; ?></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Kerajinan</td>
			<td><?= $row['kerajinan']; ?></td>
			<td>Sakit</td>
			<td><?= $row['sakit']; ?></td>
		</tr>
		<tr>
			<td>3</td>
			<td>Kebersihan dan Kerapihan</td>
			<td><?= $row['kebersihan_kerapihan']; ?></td>
			<td>Tanpa Keterangan</td>
			<td><?= $row['tanpa_keterangan']; ?></td>
		</tr>		
	</tbody>
</table>
<br>
<div class="catatan"><div class="tengah">CATATAN TENTANG PENGEMBANGAN DIRI</div><?= $row['catatan_pengembangan']; ?></div>
<br>
<div class="catatan"><div class="tengah">CATATAN</div><?= $row['catatan']; ?></div>
</div>
</body>
</html>