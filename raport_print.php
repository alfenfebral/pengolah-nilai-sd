<?php

function terbilang($x){
    $arr = array("nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    if ($x < 12)
    return " " . $arr[$x];
    elseif ($x < 20)
    return terbilang($x - 10) . " belas";
    elseif ($x < 100)
    return terbilang($x / 10) . " puluh" . terbilang($x % 10);
    elseif ($x < 200)
    return " seratus" . terbilang($x - 100);
    elseif ($x < 1000)
    return terbilang($x / 100) . " ratus" . terbilang($x % 100);
    elseif ($x < 2000)
    return " seribu" . terbilang($x - 1000);
    elseif ($x < 1000000)
    return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    elseif ($x < 1000000000)
    return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
}

function huruf_awal($kata){
	// $pieces = explode(" ", $kata);
	// $pieces[1] = ucwords($pieces[1]); 
	// $hasil = implode(" ", $pieces);
	// return $hasil;
	$replace = ucwords(substr($kata, 1,1));
	return $replace.substr($kata, 2);
}
function romawi($n){
$hasil = "";
$iromawi = array("","I","II","III","IV","V","VI","VII","VIII","IX","X",20=>"XX",30=>"XXX",40=>"XL",50=>"L",
60=>"LX",70=>"LXX",80=>"LXXX",90=>"XC",100=>"C",200=>"CC",300=>"CCC",400=>"CD",500=>"D",600=>"DC",700=>"DCC",
800=>"DCCC",900=>"CM",1000=>"M",2000=>"MM",3000=>"MMM");
if(array_key_exists($n,$iromawi)){
$hasil = $iromawi[$n];
}elseif($n >= 11 && $n <= 99){
$i = $n % 10;
$hasil = $iromawi[$n-$i] . Romawi($n % 10);
}elseif($n >= 101 && $n <= 999){
$i = $n % 100;
$hasil = $iromawi[$n-$i] . Romawi($n % 100);
}else{
$i = $n % 1000;
$hasil = $iromawi[$n-$i] . Romawi($n % 1000);
}
return $hasil;
}

?>
<?php 
include 'include/koneksi.php';

if(isset($_GET['hasil'])){
	$hasil = $_GET['hasil'];
	$sql = "SELECT * FROM `hasil_raport` INNER JOIN `mengajar` ON `mengajar`.`id_mengajar`=`hasil_raport`.`id_mengajar` INNER JOIN `siswa` ON `siswa`.`no_induk`=`mengajar`.`no_induk` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`mengajar`.`id_kelas` WHERE `hasil_raport`.`id_hasil_raport`='$hasil'";
    $query = mysql_query($sql);
    $row = mysql_fetch_assoc($query);

    // Foreach Jumlah Mata Pelajaran
    $sql = "SELECT * FROM `mapel`";
    $query = mysql_query($sql);
    $arr = array();
    $p=0;
    while ($row1 = mysql_fetch_array($query)) {
    	$arr[$p]["id_mapel"] = $row1['id_mapel'];
    	$arr[$p]["nama_mapel"] = $row1['nama_mapel'];
    	$arr[$p]["kkm"] = $row1['kkm'];
    	$arr[$p]["jenis"] = $row1['jenis_mapel'];;
    	$p++;
    }

    function myfunction($products, $field, $value)
	{
	   foreach($products as $key => $product)
	   {
	      if ( $product[$field] === $value )
	         return $key;
	   }
	   return false;
	}

    // Kalkulasi Nilai Ulangan Harian
    $id_mengajar = $row['id_mengajar'];
    $semester = $row['semester'];
    $thn_pel = $row['thn_pel'];
    // $arr_ul = array();
    // $p_ul=0;
    foreach ($arr as $arr_cek) {
    	$sql = "SELECT id_mapel, AVG(nilai) AS `nilai_rata` FROM `ulangan_harian` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`ulangan_harian`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
    	$query = mysql_query($sql);
   		$row_ul = mysql_fetch_assoc($query);
   		if(isset($row_ul['id_mapel']) && isset($row_ul['nilai_rata'])){
   			// $arr_ul[$p_ul]["id_mapel"] = $row_ul['id_mapel'];
   			// $arr_ul[$p_ul]["nilai_rata"] = $row_ul['nilai_rata'];

   			// masukkan nilai rata rata ul ke array arr
   			$key = myfunction($arr, 'id_mapel', $row_ul['id_mapel']);
   			$arr[$key]['nilai_rata_ul'] = $row_ul['nilai_rata'];
   		}
   		// $p_ul++;
    }
    // print_r($arr_ul);

    // Kalkulasi Tugas/PR
    // $arr_tgs = array();
    // $p_tgs=0;
    foreach ($arr as $arr_cek) {
    	$sql = "SELECT id_mapel, AVG(nilai) AS `nilai_rata` FROM `tugas` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`tugas`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
    	$query = mysql_query($sql);
   		$row_tgs = mysql_fetch_assoc($query);
   		if(isset($row_tgs['id_mapel']) && isset($row_tgs['nilai_rata'])){
   			// $arr_tgs[$p_tgs]["id_mapel"] = $row_tgs['id_mapel'];
   			// $arr_tgs[$p_tgs]["nilai_rata"] = $row_tgs['nilai_rata'];
   			$key = myfunction($arr, 'id_mapel', $row_tgs['id_mapel']);
   			$arr[$key]['nilai_rata_tugas'] = $row_tgs['nilai_rata'];
   		}
   		// $p_tgs++;
    }
    // print_r($arr_tgs);

    // Kalkulasi UTS
    // $arr_uts = array();
    // $p_uts=0;
    foreach ($arr as $arr_cek) {
    	$sql = "SELECT id_mapel,tulis,praktek FROM `uts` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`uts`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
    	$query = mysql_query($sql);
   		$row_uts = mysql_fetch_assoc($query);
   		if(isset($row_uts['id_mapel']) && isset($row_uts['tulis']) && isset($row_uts['praktek'])){
   			// $arr_uts[$p_uts]["id_mapel"] = $row_uts['id_mapel'];
   			// $arr_uts[$p_uts]["nilai_rata"] = ($row_uts['tulis']+$row_uts['praktek'])/2;
   			$key = myfunction($arr, 'id_mapel', $row_uts['id_mapel']);
   			$arr[$key]['nilai_rata_uts'] = ($row_uts['tulis']+$row_uts['praktek'])/2;
   		}
   		// $p_uts++;
    }
    // print_r($arr_uts);

    //Kalkulasi UAS
    // $arr_uas = array();
    // $p_uas=0;
    foreach ($arr as $arr_cek) {
    	$sql = "SELECT id_mapel,tulis,praktek FROM `uas` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`uas`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
    	$query = mysql_query($sql);
   		$row_uas = mysql_fetch_assoc($query);
   		if(isset($row_uas['id_mapel']) && isset($row_uas['tulis']) && isset($row_uas['praktek'])){
   			// $arr_uas[$p_uas]["id_mapel"] = $row_uas['id_mapel'];
   			// $arr_uas[$p_uas]["nilai_rata"] = ($row_uas['tulis']+$row_uas['praktek'])/2;
   			$key = myfunction($arr, 'id_mapel', $row_uas['id_mapel']);
   			$arr[$key]['nilai_rata_uas'] = ($row_uas['tulis']+$row_uas['praktek'])/2;
   		}
   		// $p_uas++;
    }
    // print_r($arr_uas);

    // foreach ($arr as $arr_t) {
    // 	if(in_array("mac", $arr)){
    // 		// masukkin ke array ar nilainya
    // 	}else{
    // 		// jika mapel tak ada isi value 0
    // 	}
    // }

	// echo array_search('Pendidikan Kewarganegaraan', array_keys($arr));

	// print_r($arr);

    $i=0;
	foreach ($arr as $rata) {
		if(!isset($rata['nilai_rata_ul']))
			$rata['nilai_rata_ul'] = 0;
		if(!isset($rata['nilai_rata_tugas']))
			$rata['nilai_rata_tugas'] = 0;
		if(!isset($rata['nilai_rata_uts']))
			$rata['nilai_rata_uts'] = 0;
		if(!isset($rata['nilai_rata_uas']))
			$rata['nilai_rata_uas'] = 0;
		$arr[$i]['nilai_hasil'] = ($rata['nilai_rata_ul'] + $rata['nilai_rata_tugas'] + $rata['nilai_rata_uts'] + $rata['nilai_rata_uas'])/4;
		
		$i++;
	}
	// print_r($arr);


}else{
	header("location: index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>
		Hasil Raport
	</title>
	<link rel="stylesheet" type="text/css" href="print-raport.css">
  <style type="text/css">
    .catatan{
      overflow: auto;
    }
  </style>
  <style type="text/css">
    /*@media print {
      body {transform: scale(.7);}
      table {page-break-inside: avoid;}
    }*/
    html,body{
      /*height:297mm;*/
      width:210mm;
    }
    @page { size: auto;  margin: 0mm; }
    body{
      font-size: 12px;
    }

    .header{
      margin-top: -10px;
      margin-left: 25px;
      margin-right: 25px;
    }
    .header img {
      float: left;
      width: 100px;
      height: 100px;
      background: #555;
    }

    .header h1 {
      position: relative;
      top: 18px;
      left: 10px;
      font-size: 40px;
    }
    .center{
      text-align: center;
    }
    p{
      margin-top: -10px;
      margin-bottom: 10px;
    }
    .catatan{
      height: 80px;
      width: 80%;
      margin: 0 auto;
    }
    .biodata{
      margin-left: 25px;
      margin-right: 25px;
    }
    hr{
      margin-left: 25px;
      margin-right: 25px;
    }
  </style>
</head>
<body onLoad="window.print()">
<div class="header">
<img src="logo.png" width="64px" height="64px" alt="logo">
<h1 class="center">SDN CIPARIGI BOGOR</h1>
<p class="center">Jl. Ciburial no.10 Rt 004/04 Bogor, Telp (021) 123456789</p>
<p class="center"><span style="display:inline-block; width: 250px;">Email : sdnciparigi@gmail.com</span><span style="display:inline-block; width: 250px;">Website : www.ciparigibogor.com</span></p>
</div>
<div style="clear: both;"></div>
<hr style="margin-bottom: -5px;">
<hr>
<div class="biodata" style="width: 50%">
Nomor Induk : <?= $row['no_induk']; ?><br/>
Nama Siswa : <?= $row['nama_siswa']; ?> <br/>
Kelas : <?= $row['nama_kelas']; ?><br/>
</div>
<div class="biodata" style="">
Semester : <?= romawi($row['semester']); ?><br/>
Tahun Pelajaran : <?= $row['thn_pel']; ?><br/>
</div>
<div class="clear"/>
<br/>
<table border="1" style="border-collapse: collapse;" align="center">
	<tbody>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Mata Pelajaran</th>
			<th rowspan="2" width="50px;">Kriteria Ketuntasan Minimal</th>
			<th colspan="2">Nilai</th>
		</tr>
		<tr>
			<th>Angka</th>
			<th>Huruf</th>
		</tr>
		<?php 
		$no = 0;
		?>
		<?php foreach ($arr as $arr_hsl): ?>
			<?php 
			$no++;
			?>
			<tr>
			<td class="text-center"><?= $no; ?></td>
			<td><?= $arr_hsl['nama_mapel']; ?></td>
			<td class="text-center"><?= $arr_hsl['kkm']; ?></td>
			<td class="text-center"><?= intval($arr_hsl['nilai_hasil']); ?></td>
			<td><?= huruf_awal(terbilang(intval($arr_hsl['nilai_hasil']))); ?></td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>
<br>
<table border="1" style="border-collapse: collapse;" align="center">
	<tbody>
		<tr>
			<th class="text-center">No.</th>
			<th class="text-center">Kepribadian</th>
			<th class="text-center">Nilai</th>
			<th class="text-center">Ketidakhadiran</th>
			<th class="text-center">Hari</th>
		</tr>
		<tr>
			<td>1</td>
			<td>Sikap</td>
			<td><?= $row['sikap']; ?></td>
			<td>Izin</td>
			<td><?= $row['izin']; ?></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Kerajinan</td>
			<td><?= $row['kerajinan']; ?></td>
			<td>Sakit</td>
			<td><?= $row['sakit']; ?></td>
		</tr>
		<tr>
			<td>3</td>
			<td>Kebersihan dan Kerapihan</td>
			<td><?= $row['kebersihan_kerapihan']; ?></td>
			<td>Tanpa Keterangan</td>
			<td><?= $row['tanpa_keterangan']; ?></td>
		</tr>		
	</tbody>
</table>
<br>
<div style="text-align: center;">
<div class="catatan"><div class="tengah">CATATAN TENTANG PENGEMBANGAN DIRI</div><?= $row['catatan_pengembangan']; ?></div>
<br>
<div class="catatan"><div class="tengah">CATATAN</div><?= $row['catatan']; ?></div>
</div>
</body>
</html>