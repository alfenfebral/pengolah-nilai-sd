<?php

function terbilang($x){
    $arr = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    if ($x < 12)
    return " " . $arr[$x];
    elseif ($x < 20)
    return terbilang($x - 10) . " belas";
    elseif ($x < 100)
    return terbilang($x / 10) . " puluh" . terbilang($x % 10);
    elseif ($x < 200)
    return " seratus" . terbilang($x - 100);
    elseif ($x < 1000)
    return terbilang($x / 100) . " ratus" . terbilang($x % 100);
    elseif ($x < 2000)
    return " seribu" . terbilang($x - 1000);
    elseif ($x < 1000000)
    return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    elseif ($x < 1000000000)
    return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
}

function huruf_awal($kata){
	// $pieces = explode(" ", $kata);
	// $pieces[1] = ucwords($pieces[1]); 
	// $hasil = implode(" ", $pieces);
	// return $hasil;
	$replace = ucwords(substr($kata, 1,1));
	return $replace.substr($kata, 2);
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>
		Raport
	</title>
	<link rel="stylesheet" type="text/css" href="print-raport.css">
</head>
<body>
<div class="biodata">
Nama Peserta Didik : Unknown <br/>
Nomor Induk / NISN : 0000 / 0000<br/>
Nama Sekolah : SDN Ciparigi<br/>
Alamat Sekolah : Jl. Ciburial no.10 Rt 004/04 Bogor<br/>
</div>
<div class="biodata">
Kelas : Va<br/>
Semester : 1 (Satu)<br/>
Tahun Pelajaran : 2014/2015<br/>
</div>
<div class="clear"/>
<br/>
<table border="1" style="border-collapse: collapse;">
	<tbody>
		<tr>
			<th rowspan="2">No</td>
			<th rowspan="2">Mata Pelajaran</th>
			<th rowspan="2" width="50px;">Kriteria Ketuntasan Minimal</th>
			<th colspan="2">Nilai</th>
		</tr>
		<tr>
			<th>Angka</th>
			<th>Huruf</th>
		</tr>
		<tr>
			<td class="text-center">1</td>
			<td>Pendidikan Agama</td>
			<td class="text-center">72</td>
			<td class="text-center">72</td>
			<td><?= huruf_awal(terbilang(72)); ?></td>
		</tr>
		<tr>
			<td class="text-center">2</td>
			<td>Pendidikan Kewarganegaraan</td>
			<td class="text-center">67</td>
			<td class="text-center">72</td>
			<td><?= huruf_awal(terbilang(66)); ?></td>
		</tr>
		<tr>
			<td class="text-center">3</td>
			<td>Bahasa Indonesia</td>
			<td class="text-center">67</td>
			<td class="text-center">72</td>
			<td><?= huruf_awal(terbilang(99)); ?></td>
		</tr>
		<tr>
			<td class="text-center">4</td>
			<td>Matematika</td>
			<td class="text-center">67</td>
			<td class="text-center">72</td>
			<td><?= huruf_awal(terbilang(88)); ?></td>
		</tr>
		<tr>
			<td class="text-center">5</td>
			<td>Ilmu Pengetahuan Alam</td>
			<td class="text-center">67</td>
			<td class="text-center">72</td>
			<td><?= huruf_awal(terbilang(77)); ?></td>
		</tr>
		<tr>
			<td class="text-center">6</td>
			<td>Ilmu Pengetahuan Sosial</td>
			<td class="text-center">67</td>
			<td class="text-center">72</td>
			<td><?= huruf_awal(terbilang(76)); ?></td>
		</tr>
		<tr>
			<td class="text-center">7</td>
			<td>Seni Budaya dan Keterampilan</td>
			<td class="text-center">67</td>
			<td class="text-center">72</td>
			<td><?= huruf_awal(terbilang(71)); ?></td>
		</tr>
		<tr>
			<td class="text-center">8</td>
			<td>Pendidikan Jasmani. Olahraga dan Kesehatan</td>
			<td class="text-center">67</td>
			<td class="text-center">72</td>
			<td><?= huruf_awal(terbilang(69)); ?></td>
		</tr>
		<tr>
			<td class="text-center">9</td>
			<td>Muatan Lokal:<br/>
			Bahasa dan Sastra Sunda<br/>
			PLH<br/>
			Bahasa Inggris<br/>
			</td>
			<td class="text-center">67<br/>67<br/>70</td>
			<td class="text-center">67<br/>67<br/>70</td>
			<td><?= huruf_awal(terbilang(66)); ?><br/><?= huruf_awal(terbilang(69)); ?><br/><?= huruf_awal(terbilang(64)); ?></td>
		</tr>
	</tbody>
</table>
<br>
<table border="1" style="border-collapse: collapse;">
	<tbody>
		<tr>
			<th class="text-center">No.</th>
			<th class="text-center">Kepribadian</th>
			<th class="text-center">Nilai</th>
			<th class="text-center">Ketidakhadiran</th>
			<th class="text-center">Hari</th>
		</tr>
		<tr>
			<td>1</td>
			<td>Sikap</td>
			<td>C</td>
			<td>Izin</td>
			<td>1</td>
		</tr>
		<tr>
			<td>2</td>
			<td>Kerajinan</td>
			<td>A</td>
			<td>Sakit</td>
			<td>2</td>
		</tr>
		<tr>
			<td>3</td>
			<td>Kebersihan dan Kerapihan</td>
			<td>B</td>
			<td>Tanpa Keterangan</td>
			<td>3</td>
		</tr>		
	</tbody>
</table>
<br>
<div class="catatan"><div class="tengah">CATATAN TENTANG PENGEMBANGAN DIRI</div>Pramuka : B</div>
<br>
<div class="catatan"><div class="tengah">CATATAN</div></div>
</body>
</html>