<?php 
include 'include/koneksi.php';
include 'include/fungsi.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Hasil Raport - SDN Ciparigi</title>
    <meta charset="utf-8">
    <!-- Include meta tag to ensure proper rendering and touch zooming -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Include bootstrap stylesheets -->
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <link href="./assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- Custom styles for this template -->
    <link href="./assets/css/simple.css" rel="stylesheet">
    <link href="./assets/css/materialize.css" rel="stylesheet">
    <style type="text/css">
      .cari{
        padding: 20px;
        margin: 5px;
        width: 100%;
        display:block;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        font-size: 20px;    
      }

      div.well{
        height: 250px;
      } 

      .Absolute-Center {
        margin: auto;
        position: absolute;
        top: 0; left: 0; bottom: 0; right: 0;
      }

      .Absolute-Center.is-Responsive {
        width: 100%; 
        height: 20%;
        min-width: 200px;
        max-width: 100%;
        padding: 40px;
      }

      .btn{
        margin: 0px;
      }

      .panel {
          border-radius: 3px;
          box-shadow: none;
          display: block;
      }
      .uppercase{
        text-transform: uppercase;
      }

    </style>
  </head>

  <body style="background-image: url(background.png);">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">Hasil Raport - SDN Ciparigi</a>
        </div>
      </div>
    </nav>
      <!-- Example row of columns -->
    <div class="container">
      <div class="row">
      <?php 
      if($_GET){
          $nama = $_GET['cari'];

          $limit = 10;
          if(isset($_GET['page'])){
              $page = $_GET['page'];
          }else{
              $page = 1;
          }
          $offset = ($page - 1) * $limit; 
          $sql = "SELECT * FROM `hasil_raport` INNER JOIN `mengajar` ON `mengajar`.`id_mengajar`=`hasil_raport`.`id_mengajar` INNER JOIN `siswa` ON `siswa`.`no_induk`=`mengajar`.`no_induk` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`mengajar`.`id_kelas` WHERE `siswa`.`nama_siswa` LIKE '%$nama%' ";
           $query = mysql_query($sql." LIMIT $offset, $limit");
           $total = mysql_num_rows(mysql_query($sql));
           ?>

          <div class="panel panel-default" style="margin-top: 50px;">
            <div class="panel-heading">
              <a href="./" class="btn btn-success btn-xs btn-filter"><i class="fa fa-arrow-circle-left"></i> Kembali</a> 
                <div class="pull-right">
                  <h3 class="panel-title">Data Ditemukan: <?= $total ?></h3>
                </div>
            </div>
          <div class="table-responsive">
              <table class="table table-bordered table-hover">
              <tbody>
              <thead>
                <tr class="uppercase">
                  <th>No</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Thn Pelajaran</th>
                  <th>Semester</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <?php
          if($total > 0){
            $no = 1;
               while ($row = mysql_fetch_array($query)) {

                   echo "<tr>";
                   echo "<td>".$no++."</td>";
                   echo "<td>".$row['nama_siswa']."</td>";
                   echo "<td>".$row['nama_kelas']."</td>";
                   echo "<td>".$row['thn_pel']."</td>";
                   echo "<td>".$row['semester']."</td>";
                   echo '<td><a class="btn btn-primary" href="./raport.php?hasil='.$row['id_hasil_raport'].'" role="button"><i class="fa fa-eye"></i> Hasil</a> <a class="btn btn-default" href="./raport_print.php?hasil='.$row['id_hasil_raport'].'" role="button" target="_blank"><i class="fa fa-print"></i> Print</a></td>';
                   echo "</tr>";
               }
           }else{
               echo "<tr><td colspan=\"6\">Data tidak tersedia</td></tr>";
           }
           ?>
          </tbody>
          </table>
          </div>
          </div>
      <?php
        }else{
      ?>
        <div class="Absolute-Center is-Responsive">
          <div class="col-sm-12 col-md-10 col-md-offset-1">
            <form action="index.php?nama=<?= $_GET['cari']; ?>" method="GET">
                <input class="cari" type="text" name="cari" placeholder="Masukkan Nama Siswa">
            </form>        
          </div>  
        </div>
        <?php
        }
        ?> 
      </div>
    </div>
		<!-- <hr> -->
		<footer>
			<!-- <p> &copy; 2016 SDN Ciparigi</p> -->
		</footer>
    <!-- JavaScript placed at the end of the document so the pages load faster -->
    <!-- Optional: Include the jQuery library -->
    <script src="./assets/js/jquery.js"></script>
    <!-- Optional: Incorporate the Bootstrap JavaScript plugins -->
    <script src="./assets/js/bootstrap.min.js"></script>
  </body>

</html>
