<?php 
include 'include/koneksi.php';
include 'include/fungsi.php';

if(isset($_GET['hasil'])){
  $hasil = $_GET['hasil'];
  $sql = "SELECT * FROM `hasil_raport` INNER JOIN `mengajar` ON `mengajar`.`id_mengajar`=`hasil_raport`.`id_mengajar` INNER JOIN `siswa` ON `siswa`.`no_induk`=`mengajar`.`no_induk` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`mengajar`.`id_kelas` WHERE `hasil_raport`.`id_hasil_raport`='$hasil'";
    $query = mysql_query($sql);
    $row = mysql_fetch_assoc($query);

    // Foreach Jumlah Mata Pelajaran
    $sql = "SELECT * FROM `mapel`";
    $query = mysql_query($sql);
    $arr = array();
    $p=0;
    while ($row1 = mysql_fetch_array($query)) {
      $arr[$p]["id_mapel"] = $row1['id_mapel'];
      $arr[$p]["nama_mapel"] = $row1['nama_mapel'];
      $arr[$p]["kkm"] = $row1['kkm'];
      $arr[$p]["jenis"] = $row1['jenis_mapel'];
      $p++;
    }

    function myfunction($products, $field, $value){
    foreach($products as $key => $product){
      if ( $product[$field] === $value )
         return $key;
      }
    return false;
    }

    // Kalkulasi Nilai Ulangan Harian
    $id_mengajar = $row['id_mengajar'];
    $semester = $row['semester'];
    $thn_pel = $row['thn_pel'];
    foreach ($arr as $arr_cek) {
      $sql = "SELECT id_mapel, AVG(nilai) AS `nilai_rata` FROM `ulangan_harian` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`ulangan_harian`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
      $query = mysql_query($sql);
      $row_ul = mysql_fetch_assoc($query);
      if(isset($row_ul['id_mapel']) && isset($row_ul['nilai_rata'])){

        // masukkan nilai rata rata ul ke array arr
        $key = myfunction($arr, 'id_mapel', $row_ul['id_mapel']);
        $arr[$key]['nilai_rata_ul'] = $row_ul['nilai_rata'];
      }
    }

    // Kalkulasi Tugas/PR
    foreach ($arr as $arr_cek) {
      $sql = "SELECT id_mapel, AVG(nilai) AS `nilai_rata` FROM `tugas` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`tugas`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
      $query = mysql_query($sql);
      $row_tgs = mysql_fetch_assoc($query);
      if(isset($row_tgs['id_mapel']) && isset($row_tgs['nilai_rata'])){
        $key = myfunction($arr, 'id_mapel', $row_tgs['id_mapel']);
        $arr[$key]['nilai_rata_tugas'] = $row_tgs['nilai_rata'];
      }
    }

    // Kalkulasi UTS
    foreach ($arr as $arr_cek) {
      $sql = "SELECT id_mapel,tulis,praktek FROM `uts` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`uts`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
      $query = mysql_query($sql);
      $row_uts = mysql_fetch_assoc($query);
      if(isset($row_uts['id_mapel']) && isset($row_uts['tulis']) && isset($row_uts['praktek'])){
        $key = myfunction($arr, 'id_mapel', $row_uts['id_mapel']);
        $arr[$key]['nilai_rata_uts'] = ($row_uts['tulis']+$row_uts['praktek'])/2;
      }
    }

    //Kalkulasi UAS
    foreach ($arr as $arr_cek) {
      $sql = "SELECT id_mapel,tulis,praktek FROM `uas` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`uas`.`id_daftar_nilai` WHERE `id_mengajar`='$id_mengajar' AND `daftar_nilai`.`semester`='$semester' AND `daftar_nilai`.`thn_pel`='$thn_pel' AND `daftar_nilai`.`id_mapel`='$arr_cek[id_mapel]'";
      $query = mysql_query($sql);
      $row_uas = mysql_fetch_assoc($query);
      if(isset($row_uas['id_mapel']) && isset($row_uas['tulis']) && isset($row_uas['praktek'])){
        $key = myfunction($arr, 'id_mapel', $row_uas['id_mapel']);
        $arr[$key]['nilai_rata_uas'] = ($row_uas['tulis']+$row_uas['praktek'])/2;
      }
    }

    $i=0;
  foreach ($arr as $rata) {
    if(!isset($rata['nilai_rata_ul']))
      $rata['nilai_rata_ul'] = 0;
    if(!isset($rata['nilai_rata_tugas']))
      $rata['nilai_rata_tugas'] = 0;
    if(!isset($rata['nilai_rata_uts']))
      $rata['nilai_rata_uts'] = 0;
    if(!isset($rata['nilai_rata_uas']))
      $rata['nilai_rata_uas'] = 0;
    $arr[$i]['nilai_hasil'] = ($rata['nilai_rata_ul'] + $rata['nilai_rata_tugas'] + $rata['nilai_rata_uts'] + $rata['nilai_rata_uas'])/4;
    
    $i++;
  }

}else{
  header("location: index.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Hasil Raport - SDN Ciparigi</title>
    <meta charset="utf-8">
    <!-- Include meta tag to ensure proper rendering and touch zooming -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Include bootstrap stylesheets -->
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <link href="./assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template -->
    <link href="./assets/css/simple.css" rel="stylesheet">
    <link href="./assets/css/materialize.css" rel="stylesheet">
    <style type="text/css">
      .cari{
      padding: 20px;
        margin: 5px;
        width: 100%;
        display:block;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        font-size: 20px;    
      }

      div.well{
        height: 250px;
      } 

      .Absolute-Center {
        margin: auto;
        position: absolute;
        top: 0; left: 0; bottom: 0; right: 0;
      }

      .Absolute-Center.is-Responsive {
        width: 100%; 
        height: 20%;
        min-width: 200px;
        max-width: 100%;
        padding: 40px;
      }

      .btn{
        margin: 0px;
      }

      .panel {
          border-radius: 3px;
          box-shadow: none;
          display: block;
      }
      #raport{
        /*margin-left: 5px;*/
        margin: -1px -1px;
        /*margin-right: 5px;*/
      }

      .text-center{
        text-align: center;
      }
      #catatan{
        border: 1px solid #ddd;
        height: 200px;
        padding: 5px;
      }

      #catatan .tengah{
        text-align: center;
        font-weight: bold;
        border-bottom: 1px solid #ddd;
        margin: 0px -5px 0px -5px;
      }
      #bio_container{
        border: 1px solid #ddd;
        margin: 0px 0px 0px 0px;
      }
      .biodata{
        float: left;
        width: 50%;
        padding: 5px;
      }
    </style>
  </head>

  <body>
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">Hasil Raport - SDN Ciparigi</a>
        </div>
      </div>
    </nav>
      <!-- Example row of columns -->
    <div class="container">
      <div class="row">
          <div class="panel panel-default" style="margin-top: 50px;">
            <div class="panel-heading">
              <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-success btn-xs btn-filter"><i class="fa fa-arrow-circle-left"></i> Kembali</a> 
            </div>
            <!-- Content -->
            <div id="raport">
              <div id="bio_container">
                <div class="biodata">
                <b>Nama Peserta Didik</b> : <?= $row['nama_siswa']; ?> <br/>
                <b>Nomor Induk</b> : <?= $row['no_induk']; ?><br/>
                <b>Nama Sekolah</b> : SDN Ciparigi<br/>
                <b>Alamat Sekolah</b> : Jl. Ciburial no.10 Rt 004/04 Bogor<br/>
                </div>
                <div class="biodata">
                <b>Kelas</b> : <?= $row['nama_kelas']; ?><br/>
                <b>Semester</b> : <?= romawi($row['semester']); ?> (<?= ucwords(terbilang($row['semester'])); ?>)<br/>
                <b>Tahun Pelajaran</b> : <?= $row['thn_pel']; ?><br/>
                </div>
              </div>
              <br>
              <table class="table table-bordered table-hover">
                <tbody>
                  <tr>
                    <th rowspan="2" style="vertical-align: middle;text-align: center;">No</th>
                    <th rowspan="2" style="vertical-align: middle;text-align: center;">Mata Pelajaran</th>
                    <th rowspan="2" width="50px;" style="vertical-align: middle;text-align: center;">Kriteria Ketuntasan Minimal</th>
                    <th colspan="2" style="vertical-align: middle;text-align: center;">Nilai</th>
                  </tr>
                  <tr>
                    <th style="text-align: center;">Angka</th>
                    <th style="text-align: center;">Huruf</th>
                  </tr>
                  <?php 
                  $no = 0;
                  ?>
                  <?php foreach ($arr as $arr_hsl): ?>
                    <?php 
                    $no++;
                    ?>
                    <tr>
                    <td class="text-center"><?= $no; ?></td>
                    <td><?= $arr_hsl['nama_mapel']; ?></td>
                    <td class="text-center"><?= $arr_hsl['kkm']; ?></td>
                    <td class="text-center"><?= intval($arr_hsl['nilai_hasil']); ?></td>
                    <td><?= huruf_awal(terbilang(intval($arr_hsl['nilai_hasil']))); ?></td>
                  </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
              <table class="table table-bordered table-hover">
                <tbody>
                  <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">Kepribadian</th>
                    <th class="text-center">Nilai</th>
                    <th class="text-center">Ketidakhadiran</th>
                    <th class="text-center">Hari</th>
                  </tr>
                  <tr>
                    <td class="text-center">1</td>
                    <td>Sikap</td>
                    <td class="text-center"><?= $row['sikap']; ?></td>
                    <td>Izin</td>
                    <td class="text-center"><?= $row['izin']; ?></td>
                  </tr>
                  <tr>
                    <td class="text-center">2</td>
                    <td>Kerajinan</td>
                    <td class="text-center"><?= $row['kerajinan']; ?></td>
                    <td>Sakit</td>
                    <td class="text-center"><?= $row['sakit']; ?></td>
                  </tr>
                  <tr>
                    <td class="text-center">3</td>
                    <td>Kebersihan dan Kerapihan</td>
                    <td class="text-center"><?= $row['kebersihan_kerapihan']; ?></td>
                    <td>Tanpa Keterangan</td>
                    <td class="text-center"><?= $row['tanpa_keterangan']; ?></td>
                  </tr>   
                </tbody>
              </table>
              <div id="catatan">
                <div class="tengah">CATATAN TENTANG PENGEMBANGAN DIRI</div>
                <?= $row['catatan_pengembangan']; ?>
              </div>
              <br>
              <div id="catatan"><div class="tengah">CATATAN</div><?= $row['catatan']; ?></div>
            </div>
          </div>
      </div>
    </div>
    <!-- <hr> -->
    <footer>
      <!-- <p> &copy; 2016 SDN Ciparigi</p> -->
    </footer>
    <!-- JavaScript placed at the end of the document so the pages load faster -->
    <!-- Optional: Include the jQuery library -->
    <script src="./assets/js/jquery.js"></script>
    <!-- Optional: Incorporate the Bootstrap JavaScript plugins -->
    <script src="./assets/js/bootstrap.min.js"></script>
  </body>

</html>
