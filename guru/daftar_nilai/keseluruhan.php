<?php
session_start();
if(!isset($_SESSION['id_guru']) || !isset($_SESSION['nama_guru']) || !isset($_SESSION['username'])){
    header("location: ../login.php");
}
include '../../include/koneksi.php';
include '../../include/fungsi.php';

// Proses Tambah
if(isset($_POST['tambah_ul'])){
	$id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $ulangan_ke = $_POST['idUl'];
    $nilai = $_POST['nilai'];
    $sql = "INSERT INTO `ulangan_harian`(`id_mengajar`, `id_daftar_nilai`, `ulangan_ke`, `nilai`) VALUES ('$id_mengajar','$id_daftar_nilai','$ulangan_ke','$nilai')";
    $query = mysql_query($sql);

	if($query){
		// echo '<script>alert("Input Data Berhasil");</script>';
	}else{
		echo '<script>alert("Input Data Gagal");</script>';
	}
}
if(isset($_POST['tambah_tgs'])){
	$id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $ulangan_ke = $_POST['idUl'];
    $nilai = $_POST['nilai'];
    $sql = "INSERT INTO `tugas`(`id_mengajar`, `id_daftar_nilai`, `ulangan_ke`, `nilai`) VALUES ('$id_mengajar','$id_daftar_nilai','$ulangan_ke','$nilai')";
    $query = mysql_query($sql);

	if($query){
		// echo '<script>alert("Input Data Berhasil");</script>';
	}else{
		echo '<script>alert("Input Data Gagal");</script>';
	}
}
if(isset($_POST['tambah_uts'])){
	$id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $tulis = $_POST['tulis'];
    $praktek = $_POST['praktek'];
    $sql = "INSERT INTO `uts`(`id_mengajar`, `id_daftar_nilai`, `tulis`, `praktek`) VALUES ('$id_mengajar','$id_daftar_nilai','$tulis','$praktek')";
    $query = mysql_query($sql);

	if($query){
		// echo '<script>alert("Input Data Berhasil");</script>';
	}else{
		echo '<script>alert("Input Data Gagal");</script>';
	}
}
if(isset($_POST['tambah_uas'])){
	$id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $tulis = $_POST['tulis'];
    $praktek = $_POST['praktek'];
    $sql = "INSERT INTO `uas`(`id_mengajar`, `id_daftar_nilai`, `tulis`, `praktek`) VALUES ('$id_mengajar','$id_daftar_nilai','$tulis','$praktek')";
    $query = mysql_query($sql);

	if($query){
		// echo '<script>alert("Input Data Berhasil");</script>';
	}else{
		echo '<script>alert("Input Data Gagal");</script>';
	}
}
////////////////////
if(isset($_POST['edit_ul'])){
	$id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $ulangan_ke = $_POST['idUl'];
    $nilai = $_POST['nilai'];
    $sql = "UPDATE `ulangan_harian` SET `nilai`='$nilai' WHERE `id_mengajar`='$id_mengajar' AND `id_daftar_nilai`='$id_daftar_nilai' AND `ulangan_ke`='$ulangan_ke'";
    $query = mysql_query($sql);

	if($query){
		// echo '<script>alert("Input Data Berhasil");</script>';
	}else{
		echo '<script>alert("Input Data Gagal");</script>';
	}
}
if(isset($_POST['delete_ul'])){
	$id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $ulangan_ke = $_POST['idUl'];
    $nilai = $_POST['nilai'];
    $sql = "DELETE FROM `ulangan_harian` WHERE `nilai`='$nilai' AND `id_mengajar`='$id_mengajar' AND `id_daftar_nilai`='$id_daftar_nilai' AND `ulangan_ke`='$ulangan_ke'";
    $query = mysql_query($sql);

	if($query){
		// echo '<script>alert("Input Data Berhasil");</script>';
	}else{
		echo '<script>alert("Input Data Gagal");</script>';
	}
}
if(isset($_POST['edit_tgs'])){
    $id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $ulangan_ke = $_POST['idUl'];
    $nilai = $_POST['nilai'];
    $sql = "UPDATE `tugas` SET `nilai`='$nilai' WHERE `id_mengajar`='$id_mengajar' AND `id_daftar_nilai`='$id_daftar_nilai' AND `ulangan_ke`='$ulangan_ke'";
    $query = mysql_query($sql);

    if($query){
        // echo '<script>alert("Input Data Berhasil");</script>';
    }else{
        echo '<script>alert("Input Data Gagal");</script>';
    }
}
if(isset($_POST['delete_tgs'])){
    $id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $ulangan_ke = $_POST['idUl'];
    $nilai = $_POST['nilai'];
    $sql = "DELETE FROM `tugas` WHERE `nilai`='$nilai' AND `id_mengajar`='$id_mengajar' AND `id_daftar_nilai`='$id_daftar_nilai' AND `ulangan_ke`='$ulangan_ke'";
    $query = mysql_query($sql);

    if($query){
        // echo '<script>alert("Input Data Berhasil");</script>';
    }else{
        echo '<script>alert("Input Data Gagal");</script>';
    }
}

if(isset($_POST['edit_uts'])){
    $id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $tulis = $_POST['tulis'];
    $praktek = $_POST['praktek'];
    $sql = "UPDATE `uts` SET `tulis`='$tulis',`praktek`='$praktek' WHERE `id_mengajar`='$id_mengajar' AND `id_daftar_nilai`='$id_daftar_nilai'";
    $query = mysql_query($sql);

    if($query){
        // echo '<script>alert("Input Data Berhasil");</script>';
    }else{
        echo '<script>alert("Input Data Gagal");</script>';
    }
}
if(isset($_POST['delete_uts'])){
    $id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $tulis = $_POST['tulis'];
    $praktek = $_POST['praktek'];
    $sql = "DELETE FROM `uts` WHERE `id_mengajar`='$id_mengajar' AND `id_daftar_nilai`='$id_daftar_nilai'";
    $query = mysql_query($sql);

    if($query){
        // echo '<script>alert("Input Data Berhasil");</script>';
    }else{
        echo '<script>alert("Input Data Gagal");</script>';
    }
}

if(isset($_POST['edit_uas'])){
    $id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $tulis = $_POST['tulis'];
    $praktek = $_POST['praktek'];
    $sql = "UPDATE `uas` SET `tulis`='$tulis',`praktek`='$praktek' WHERE `id_mengajar`='$id_mengajar' AND `id_daftar_nilai`='$id_daftar_nilai'";
    $query = mysql_query($sql);

    if($query){
        // echo '<script>alert("Input Data Berhasil");</script>';
    }else{
        echo '<script>alert("Input Data Gagal");</script>';
    }
}
if(isset($_POST['delete_uas'])){
    $id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $tulis = $_POST['tulis'];
    $praktek = $_POST['praktek'];
    $sql = "DELETE FROM `uas` WHERE `id_mengajar`='$id_mengajar' AND `id_daftar_nilai`='$id_daftar_nilai'";
    $query = mysql_query($sql);

    if($query){
        // echo '<script>alert("Input Data Berhasil");</script>';
    }else{
        echo '<script>alert("Input Data Gagal");</script>';
    }
}
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Daftar Nilai - Keseluruhan</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../">Pengolah Nilai - Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $_SESSION['nama_guru']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="../profil"><i class="fa fa-fw fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="../ubah_password"><i class="fa fa-fw fa-gear"></i> Ubah Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="../"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="../mengajar"><i class="fa fa-fw fa-user"></i> Mengajar</a>
                    </li>
                    <li class="active">
                        <a href="../daftar_nilai"><i class="fa fa-fw fa-star"></i> Daftar Nilai</a>
                    </li>
                    <!-- <li>
                        <a href="../ulangan_harian"><i class="fa fa-fw fa-file"></i> Ulangan harian</a>
                    </li>
                    <li>
                        <a href="../tugas"><i class="fa fa-fw fa-file"></i> Tugas/PR</a>
                    </li>
                    <li>
                        <a href="../uts"><i class="fa fa-fw fa-file"></i> UTS</a>
                    </li>
                    
                    <li>
                        <a href="../uas"><i class="fa fa-fw fa-file"></i> UAS/UKK</a>
                    </li> -->
                    <li>
                        <a href="../hasil_raport"><i class="fa fa-fw fa-file"></i> Hasil Raport</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Daftar Nilai
                            <small>Data Daftar Nilai Keseluruhan</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="../../admin">Dashboard</a>
                            </li>
                            <li>
                                <i class="fa fa-star"></i> <a href="../daftar_nilai">Daftar Nilai</a>
                            </li></a>
                            <li class="active">
                                <i class="fa fa-list"></i> Detail
                            </li>
                        </ol>
                        
                        <form class="form-inline" action="index.php" method="post">
                            <!-- <a href="./tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
                            <a href="./print.php" class="btn btn-warning"><i class="fa fa-print"></i> Print</a> -->
                        </form>
                        <?php
                        $sql_desc = "SELECT * FROM `daftar_nilai` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`daftar_nilai`.`id_kelas` INNER JOIN `mapel` ON `mapel`.`id_mapel`=`daftar_nilai`.`id_mapel` INNER JOIN `guru` ON `guru`.`nip_guru`=`daftar_nilai`.`nip_guru` WHERE `daftar_nilai`.`id_daftar_nilai`='$_GET[id]'";
                        $query_desc = mysql_query($sql_desc);
                        $desc = mysql_fetch_assoc($query_desc);
                        echo '<h4>Mapel : '.$desc['nama_mapel'].'</h4>';
                        echo '<h4>Kelas : '.$desc['nama_kelas'].'</h4>';
                        echo '<h4>Nama Guru : '.$desc['nama_guru'].'</h4>';
                        echo '<h4>Semester : '.$desc['semester'].'</h4>';
                        echo '<h4>Thn Pelajaran : '.$desc['thn_pel'].'</h4>';
                        ?>

                        <?php
                        // $limit = 8;
                        // if(isset($_GET['page'])){
                        //     $page = $_GET['page'];
                        // }else{
                        //     $page = 1;
                        // }
                        // $offset = ($page - 1) * $limit; 
                        $sql = "SELECT * FROM `mengajar` INNER JOIN `siswa` ON `siswa`.`no_induk`=`mengajar`.`no_induk` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`mengajar`.`id_kelas` WHERE `nama_kelas`='$desc[nama_kelas]' AND `nip_guru`='$desc[nip_guru]'";
                         // $query = mysql_query($sql." LIMIT $offset, $limit");
                         $query = mysql_query($sql);
                         $total = mysql_num_rows(mysql_query($sql));
                        ?>

                        <h4>Total Data: <?= $total; ?></h4>
                        <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            	<tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2" >Nama Siswa</th>
                                <th colspan="8" style="text-align: center;">Ulangan Harian</th>
                                <th colspan="8" style="text-align: center;">Tugas/PR</th>
                                <th colspan="2" style="text-align: center;">UTS</th>
                                <th colspan="2" style="text-align: center;">UAS</th>
                                </tr>
                                <tr>
                                <!-- Ulangan Harian -->
                            	<th>1</th>
								<th>2</th>
								<th>3</th>
								<th>4</th>
								<th>5</th>
								<th>6</th>
								<th>7</th>
								<th>8</th>

								<!-- Tugas/PR -->
								<th>1</th>
								<th>2</th>
								<th>3</th>
								<th>4</th>
								<th>5</th>
								<th>6</th>
								<th>7</th>
								<th>8</th>

								<th>1</th>
								<th>2</th>

								<th>1</th>
								<th>2</th>
								</tr>
                            </thead>
                        <?php
                        $no_all=1;
                        if($total > 0){
                             while ($row = mysql_fetch_array($query)) {
                                 echo "<tr>";
                                 echo "<td>".$no_all++."</td>";
                                 echo "<td>".$row['nama_siswa']."</td>";

                                 // Bagian Ulangan Harian
                                 $sql_ul = "SELECT * FROM `ulangan_harian` WHERE `id_daftar_nilai`='$_GET[id]' AND `id_mengajar`='$row[id_mengajar]'";
                                 $query_ul = mysql_query($sql_ul);
                                 $total_ul = mysql_num_rows($query_ul);
                                 if($total_ul>0){
	                             	$arr_ul = array();
    								$p_ul=0;
	                                 while ($row_ul = mysql_fetch_array($query_ul)) {
	                                 	$arr_ul[$p_ul]["ulangan_ke"] = $row_ul['ulangan_ke'];
	                                 	$arr_ul[$p_ul]["nilai"] = $row_ul['nilai'];
	                                 	$p_ul++;
	                                 }
	                                for ($i=0; $i < 8; $i++) { 
		                             	$no=$i+1;
		                             	$key = array_search($no, array_column($arr_ul, 'ulangan_ke'));
		                             	if($no == $arr_ul[$key]['ulangan_ke'])
		                          			echo '<td><a href="" style="color: black" class="ulEdit" data-toggle="modal" data-target="#ulEditModal" data-id="'.$no.'" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'" data-nilai="'.$arr_ul[$key]['nilai'].'">'.$arr_ul[$key]['nilai'].'</a></td>';
		                          		else
		                          			echo '<td><button type="button" class="ulTambah btn btn-primary btn-xs" data-toggle="modal" data-target="#ulModal" data-id="'.$no.'" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'">+</button></td>';
	                             	}
	                             }else{
                                    $no=0;
	                             	for ($i=0; $i < 8; $i++) { 
                                        $no++;
	                             		echo '<td><button type="button" class="ulTambah btn btn-primary btn-xs" data-toggle="modal" data-target="#ulModal" data-id="'.$no.'" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'">+</button></td>';
	                             	}
	                             }
	                             unset($arr_ul); // clear array value

	                             //Bagian Tugas/PR
	                             $sql_tgs = "SELECT * FROM `tugas` WHERE `id_daftar_nilai`='$_GET[id]' AND `id_mengajar`='$row[id_mengajar]'";
                                 $query_tgs = mysql_query($sql_tgs);
                                 $total_tgs = mysql_num_rows($query_tgs);
                                 if($total_tgs>0){
	                             	$arr_tgs = array();
    								$p_tgs=0;
	                                 while ($row_tgs = mysql_fetch_array($query_tgs)) {
	                                 	$arr_tgs[$p_tgs]["ulangan_ke"] = $row_tgs['ulangan_ke'];
	                                 	$arr_tgs[$p_tgs]["nilai"] = $row_tgs['nilai'];
	                                 	$p_tgs++;
	                                 }
	                                for ($i=0; $i < 8; $i++) { 
		                             	$no=$i+1;
		                             	$key = array_search($no, array_column($arr_tgs, 'ulangan_ke'));
		                             	if($no == $arr_tgs[$key]['ulangan_ke'])
		                          			echo '<td><a href="" style="color: black" class="tgsEdit" data-toggle="modal" data-target="#tgsEditModal" data-id="'.$no.'" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'" data-nilai="'.$arr_tgs[$key]['nilai'].'">'.$arr_tgs[$key]['nilai'].'</a></td>';
		                          		else
		                          			echo '<td><button type="button" class="tgsTambah btn btn-primary btn-xs" data-toggle="modal" data-target="#tgsModal" data-id="'.$no.'" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'">+</button></td>';
	                             	}
	                             }else{
                                    $no=0;
	                             	for ($i=0; $i < 8; $i++) {
	                             		$no++;
	                             		echo '<td><button type="button" class="tgsTambah btn btn-primary btn-xs" data-toggle="modal" data-target="#tgsModal" data-id="'.$no.'" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'">+</button></td>';
	                             	}
	                             }
	                             unset($arr_tgs); // clear array value

	                             // Bagian UTS
	                             $sql_uts = "SELECT * FROM `uts` WHERE `id_daftar_nilai`='$_GET[id]' AND `id_mengajar`='$row[id_mengajar]'";
                                 $query_uts = mysql_query($sql_uts);
                                 $total_uts = mysql_num_rows($query_uts);
                                 if($total_uts>0){
	                                 while ($row_uts = mysql_fetch_array($query_uts)) {
                                        // echo '<td>'.$row_uts['tulis'].'</td>';
	                                 	echo '<td><a href="" style="color: black" class="utsEdit" data-toggle="modal" data-target="#utsEditModal" data-id="'.$no.'" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'" data-tulis="'.$row_uts['tulis'].'" data-praktek="'.$row_uts['praktek'].'">'.$row_uts['tulis'].'</a></td>';
	                                 	echo '<td><a href="" style="color: black" class="utsEdit" data-toggle="modal" data-target="#utsEditModal" data-id="'.$no.'" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'" data-tulis="'.$row_uts['tulis'].'" data-praktek="'.$row_uts['praktek'].'">'.$row_uts['praktek'].'</a></td>';
	                                 }
	                             }else{
	                             	for ($i=0; $i < 2; $i++) { 
	                             		echo '<td><button type="button" class="utsTambah btn btn-primary btn-xs" data-toggle="modal" data-target="#utsModal" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'">+</button></td>';
	                             	}
	                             }

	                             // Bagian UAS
	                             $sql_uts = "SELECT * FROM `uas` WHERE `id_daftar_nilai`='$_GET[id]' AND `id_mengajar`='$row[id_mengajar]'";
                                 $query_uts = mysql_query($sql_uts);
                                 $total_uts = mysql_num_rows($query_uts);
                                 if($total_uts>0){
	                                 while ($row_uts = mysql_fetch_array($query_uts)) {
	                                 	echo '<td><a href="" style="color: black" class="uasEdit" data-toggle="modal" data-target="#uasEditModal" data-id="'.$no.'" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'" data-tulis="'.$row_uts['tulis'].'" data-praktek="'.$row_uts['praktek'].'">'.$row_uts['tulis'].'</a></td>';
	                                 	echo '<td><a href="" style="color: black" class="uasEdit" data-toggle="modal" data-target="#uasEditModal" data-id="'.$no.'" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'" data-tulis="'.$row_uts['tulis'].'" data-praktek="'.$row_uts['praktek'].'">'.$row_uts['praktek'].'</a></td>';
	                                 }
	                             }else{
	                             	for ($i=0; $i < 2; $i++) { 
	                             		echo '<td><button type="button" class="uasTambah btn btn-primary btn-xs" data-toggle="modal" data-target="#uasModal" data-mengajar="'.$row['id_mengajar'].'" data-daftarnilai="'.$_GET['id'].'">+</button></td>';
	                             	}
	                             }
	                             
                                 //echo '<td><a href="edit.php?id='.$row['id_siswa'].'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a> <a class="btn btn-sm btn-danger" href="hapus.php?id='.$row['id_siswa'].'" title="Hapus"><i class="fa fa-trash"></i> Delete</a></td>';
                                 echo "</tr>";
                             }
                         }else{
                             echo "<tr><td colspan=\"6\">Data tidak tersedia</td></tr>";
                         }
                        ?>
                        </table>
                        </div>
                        <div class="col-lg-12 text-center"><?php //pagination_new($total, $page, $limit, '#', '?page='); ?>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
            <hr>
            <p style="font-weight: bold">
                © 2016 <a href="http://larfess.com" target="_blank">Messias Alfen Febral</a>
            </p>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Modal -->
	<div id="ulModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	    <form action="keseluruhan.php?id=<?= $_GET['id']; ?>" method="POST">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Tambah Ulangan Harian</h4>
	      </div>
	      <div class="modal-body">
	        <input type="hidden" class="form-control" name="idUl" id="idUl" value=""/>
	        <input type="hidden" class="form-control" name="id_mengajar" id="id_mengajar" value=""/>
	        <input type="hidden" class="form-control" name="id_daftar_nilai" id="id_daftar_nilai" value=""/>
	        <div class="form-group">
	            <label>Nilai</label>
	            <div class="row">
	                <div class="col-lg-5">
	                    <input class="form-control" name="nilai" placeholder="000" maxlength="3">
	                </div>
	            </div>
	        </div>
	      </div>
	      <div class="modal-footer">
            <button type="submit" name="tambah_ul" class="btn btn-primary">Tambah</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	      </form>
	    </div>

	  </div>
	</div>

	<div id="tgsModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	    <form action="keseluruhan.php?id=<?= $_GET['id']; ?>" method="POST">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Tambah Tugas/PR</h4>
	      </div>
	      <div class="modal-body">
	        <input type="hidden" class="form-control" name="idUl" id="idUl" value=""/>
	        <input type="hidden" class="form-control" name="id_mengajar" id="id_mengajar" value=""/>
	        <input type="hidden" class="form-control" name="id_daftar_nilai" id="id_daftar_nilai" value=""/>
	        <div class="form-group">
	            <label>Nilai</label>
	            <div class="row">
	                <div class="col-lg-5">
	                    <input class="form-control" name="nilai" placeholder="000" maxlength="3">
	                </div>
	            </div>
	        </div>
	      </div>
	      <div class="modal-footer">
            <button type="submit" name="tambah_tgs" class="btn btn-primary">Tambah</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	      </form>
	    </div>

	  </div>
	</div>

	<div id="utsModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	    <form action="keseluruhan.php?id=<?= $_GET['id']; ?>" method="POST">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">UTS</h4>
	      </div>
	      <div class="modal-body">
	        <input type="hidden" class="form-control" name="id_mengajar" id="id_mengajar" value=""/>
	        <input type="hidden" class="form-control" name="id_daftar_nilai" id="id_daftar_nilai" value=""/>
	        <div class="form-group">
	            <label>Nilai Tulis</label>
	            <div class="row">
	                <div class="col-lg-5">
	                    <input class="form-control" name="tulis" placeholder="000" maxlength="3">
	                </div>
	            </div>
	        </div>
	        <div class="form-group">
	            <label>Nilai Praktek</label>
	            <div class="row">
	                <div class="col-lg-5">
	                    <input class="form-control" name="praktek" placeholder="000" maxlength="3">
	                </div>
	            </div>
	        </div>
	      </div>
	      <div class="modal-footer">
            <button type="submit" name="tambah_uts" class="btn btn-primary">Tambah</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	      </form>
	    </div>

	  </div>
	</div>

	<div id="uasModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	    <form action="keseluruhan.php?id=<?= $_GET['id']; ?>" method="POST">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">UTS</h4>
	      </div>
	      <div class="modal-body">
	        <input type="hidden" class="form-control" name="id_mengajar" id="id_mengajar" value=""/>
	        <input type="hidden" class="form-control" name="id_daftar_nilai" id="id_daftar_nilai" value=""/>
	        <div class="form-group">
	            <label>Nilai Tulis</label>
	            <div class="row">
	                <div class="col-lg-5">
	                    <input class="form-control" name="tulis" placeholder="000" maxlength="3">
	                </div>
	            </div>
	        </div>
	        <div class="form-group">
	            <label>Nilai Praktek</label>
	            <div class="row">
	                <div class="col-lg-5">
	                    <input class="form-control" name="praktek" placeholder="000" maxlength="3">
	                </div>
	            </div>
	        </div>
	      </div>
	      <div class="modal-footer">
            <button type="submit" name="tambah_uas" class="btn btn-primary">Tambah</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	      </form>
	    </div>

	  </div>
	</div>
<!-- Edit -->
	<div id="ulEditModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	    <form action="keseluruhan.php?id=<?= $_GET['id']; ?>" method="POST">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Edit Ulangan Harian</h4>
	      </div>
	      <div class="modal-body">
	        <input type="hidden" class="form-control" name="idUl" id="idUl" value=""/>
	        <input type="hidden" class="form-control" name="id_mengajar" id="id_mengajar" value=""/>
	        <input type="hidden" class="form-control" name="id_daftar_nilai" id="id_daftar_nilai" value=""/>
	        <div class="form-group">
	            <label>Nilai</label>
	            <div class="row">
	                <div class="col-lg-5">
	                    <input class="form-control" name="nilai" id="nilai" placeholder="000" maxlength="3">
	                </div>
	            </div>
	        </div>
	      </div>
	      <div class="modal-footer">
            <button type="submit" name="edit_ul" class="btn btn-primary">Perbaharui</button>
            <button type="submit" name="delete_ul" class="btn btn-danger">Hapus</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
	      </div>
	      </form>
	    </div>

	  </div>
	</div> 

    <div id="tgsEditModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <form action="keseluruhan.php?id=<?= $_GET['id']; ?>" method="POST">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Tugas/PR</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" class="form-control" name="idUl" id="idUl" value=""/>
            <input type="hidden" class="form-control" name="id_mengajar" id="id_mengajar" value=""/>
            <input type="hidden" class="form-control" name="id_daftar_nilai" id="id_daftar_nilai" value=""/>
            <div class="form-group">
                <label>Nilai</label>
                <div class="row">
                    <div class="col-lg-5">
                        <input class="form-control" name="nilai" id="nilai" placeholder="000" maxlength="3">
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" name="edit_tgs" class="btn btn-primary">Perbaharui</button>
            <button type="submit" name="delete_tgs" class="btn btn-danger">Hapus</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
          </div>
          </form>
        </div>

      </div>
    </div>

    <div id="utsEditModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <form action="keseluruhan.php?id=<?= $_GET['id']; ?>" method="POST">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">UTS</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" class="form-control" name="id_mengajar" id="id_mengajar" value=""/>
            <input type="hidden" class="form-control" name="id_daftar_nilai" id="id_daftar_nilai" value=""/>
            <div class="form-group">
                <label>Nilai Tulis</label>
                <div class="row">
                    <div class="col-lg-5">
                        <input class="form-control" name="tulis" id="tulis" placeholder="000" maxlength="3">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Nilai Praktek</label>
                <div class="row">
                    <div class="col-lg-5">
                        <input class="form-control" name="praktek" id="praktek" placeholder="000" maxlength="3">
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" name="edit_uts" class="btn btn-primary">Perbaharui</button>
            <button type="submit" name="delete_uts" class="btn btn-danger">Hapus</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          </form>
        </div>

      </div>
    </div>

    <div id="uasEditModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <form action="keseluruhan.php?id=<?= $_GET['id']; ?>" method="POST">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">UTS</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" class="form-control" name="id_mengajar" id="id_mengajar" value=""/>
            <input type="hidden" class="form-control" name="id_daftar_nilai" id="id_daftar_nilai" value=""/>
            <div class="form-group">
                <label>Nilai Tulis</label>
                <div class="row">
                    <div class="col-lg-5">
                        <input class="form-control" name="tulis" id="tulis" placeholder="000" maxlength="3">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Nilai Praktek</label>
                <div class="row">
                    <div class="col-lg-5">
                        <input class="form-control" name="praktek" id="praktek" placeholder="000" maxlength="3">
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" name="edit_uas" class="btn btn-primary">Perbaharui</button>
            <button type="submit" name="delete_uas" class="btn btn-danger">Hapus</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          </form>
        </div>

      </div>
    </div> 

    <!-- jQuery -->
    <script src="../../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../assets/js/bootstrap.min.js"></script>

    <!-- Modal data -->
    <script type="text/javascript">
    	$(document).on("click", ".ulTambah", function () {
		     var id = $(this).data('id');
		     var id_mengajar = $(this).data('mengajar');
		     var id_daftar_nilai = $(this).data('daftarnilai');
		     $(".modal-body #idUl").val(id);
		     $(".modal-body #id_mengajar").val(id_mengajar);
		     $(".modal-body #id_daftar_nilai").val(id_daftar_nilai);
		});
		$(document).on("click", ".tgsTambah", function () {
		     var id = $(this).data('id');
		     var id_mengajar = $(this).data('mengajar');
		     var id_daftar_nilai = $(this).data('daftarnilai');
		     $(".modal-body #idUl").val(id);
		     $(".modal-body #id_mengajar").val(id_mengajar);
		     $(".modal-body #id_daftar_nilai").val(id_daftar_nilai);
		});
		$(document).on("click", ".utsTambah", function () {
		     var id_mengajar = $(this).data('mengajar');
		     var id_daftar_nilai = $(this).data('daftarnilai');
		     $(".modal-body #id_mengajar").val(id_mengajar);
		     $(".modal-body #id_daftar_nilai").val(id_daftar_nilai);
		});
		$(document).on("click", ".uasTambah", function () {
		     var id_mengajar = $(this).data('mengajar');
		     var id_daftar_nilai = $(this).data('daftarnilai');
		     $(".modal-body #id_mengajar").val(id_mengajar);
		     $(".modal-body #id_daftar_nilai").val(id_daftar_nilai);
		});
		////////////////////////////////Edit
		$(document).on("click", ".ulEdit", function () {
			 var id = $(this).data('id');
		     var id_mengajar = $(this).data('mengajar');
		     var id_daftar_nilai = $(this).data('daftarnilai');
		     var nilai = $(this).data('nilai');
		     $(".modal-body #idUl").val(id);
		     $(".modal-body #id_mengajar").val(id_mengajar);
		     $(".modal-body #id_daftar_nilai").val(id_daftar_nilai);
		     $(".modal-body #nilai").val(nilai);
		});

        $(document).on("click", ".tgsEdit", function () {
             var id = $(this).data('id');
             var id_mengajar = $(this).data('mengajar');
             var id_daftar_nilai = $(this).data('daftarnilai');
             var nilai = $(this).data('nilai');
             $(".modal-body #idUl").val(id);
             $(".modal-body #id_mengajar").val(id_mengajar);
             $(".modal-body #id_daftar_nilai").val(id_daftar_nilai);
             $(".modal-body #nilai").val(nilai);
        });

        $(document).on("click", ".utsEdit", function () {
             var id_mengajar = $(this).data('mengajar');
             var id_daftar_nilai = $(this).data('daftarnilai');
             var tulis = $(this).data('tulis');
             var praktek = $(this).data('praktek');
             $(".modal-body #id_mengajar").val(id_mengajar);
             $(".modal-body #id_daftar_nilai").val(id_daftar_nilai);
             $(".modal-body #tulis").val(tulis);
             $(".modal-body #praktek").val(praktek);
        });

        $(document).on("click", ".uasEdit", function () {
             var id_mengajar = $(this).data('mengajar');
             var id_daftar_nilai = $(this).data('daftarnilai');
             var tulis = $(this).data('tulis');
             var praktek = $(this).data('praktek');
             $(".modal-body #id_mengajar").val(id_mengajar);
             $(".modal-body #id_daftar_nilai").val(id_daftar_nilai);
             $(".modal-body #tulis").val(tulis);
             $(".modal-body #praktek").val(praktek);
        });
    </script>

</body>

</html>