<?php 
session_start();
if(!isset($_SESSION['id_guru']) || !isset($_SESSION['nama_guru']) || !isset($_SESSION['username'])){
    header("location: ../login.php");
}
include '../../include/koneksi.php';
include '../../include/fungsi.php';
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tugas/PR</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../">Pengolah Nilai - Guru</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $_SESSION['nama_guru']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="../profil"><i class="fa fa-fw fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="../ubah_password"><i class="fa fa-fw fa-gear"></i> Ubah Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="../"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="../mengajar"><i class="fa fa-fw fa-user"></i> Mengajar</a>
                    </li>
                    <li>
                        <a href="../daftar_nilai"><i class="fa fa-fw fa-star"></i> Daftar Nilai</a>
                    </li>
                    <li>
                        <a href="../ulangan_harian"><i class="fa fa-fw fa-file"></i> Ulangan harian</a>
                    </li>
                    <li class="active">
                        <a href="../tugas"><i class="fa fa-fw fa-file"></i> Tugas/PR</a>
                    </li>
                    <li>
                        <a href="../uts"><i class="fa fa-fw fa-file"></i> UTS</a>
                    </li>
                    
                    <li>
                        <a href="../uas"><i class="fa fa-fw fa-file"></i> UAS/UKK</a>
                    </li>
                    <li>
                        <a href="../hasil_raport"><i class="fa fa-fw fa-file"></i> Hasil Raport</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Tugas/PR
                            <small>Data Tugas/PR</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="../../guru">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Tugas/PR
                            </li>
                        </ol>
                        
                        <form class="form-inline" action="index.php" method="post">
                            <a href="./tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
                            <!-- <a href="./print.php" class="btn btn-warning"><i class="fa fa-print"></i> Print</a> -->
                        </form>

                        <?php
                        $limit = 10;
                        if(isset($_GET['page'])){
                            $page = $_GET['page'];
                        }else{
                            $page = 1;
                        }
                        $offset = ($page - 1) * $limit; 
                        $sql = "SELECT * FROM `tugas` INNER JOIN `mengajar` ON `mengajar`.`id_mengajar`=`tugas`.`id_mengajar` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`tugas`.`id_daftar_nilai` INNER JOIN `mapel` ON `mapel`.`id_mapel`=`daftar_nilai`.`id_mapel` INNER JOIN `siswa` ON `siswa`.`id_siswa`=`mengajar`.`id_siswa` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`mengajar`.`id_kelas` WHERE `mengajar`.`id_guru`='$_SESSION[id_guru]'";
                         $query = mysql_query($sql." LIMIT $offset, $limit");
                         $total = mysql_num_rows(mysql_query($sql));
                        ?>

                        <h4>Total Data: <?= $total; ?></h4>
                        <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <th>No</th>
                                <th>Nama Siswa</th>
                                <th>Nama Mapel</th>
                                <th>Ke</th>
                                <th>Nilai</th>
                                <th>Aksi</th>
                            </thead>
                        <?php 
                        if($total > 0){
                            $no = ($page - 1) * $limit; 
                             while ($row = mysql_fetch_array($query)) {
                                $no++;
                                 echo "<tr>";
                                 echo "<td>".$no."</td>";
                                 echo "<td>".$row['nama_siswa']."[".$row['nama_kelas']."]</td>";
                                 echo "<td>".$row['nama_mapel']."</td>";
                                 echo "<td>".$row['ulangan_ke']."</td>";
                                 echo "<td>".$row['nilai']."</td>";
                                 echo '<td><a href="edit.php?id='.$row['id_tugas'].'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a> <a class="btn btn-sm btn-danger" href="hapus.php?id='.$row['id_tugas'].'" title="Hapus"><i class="fa fa-trash"></i> Delete</a></td>';
                                 echo "</tr>";
                             }
                         }else{
                             echo "<tr><td colspan=\"6\">Data tidak tersedia</td></tr>";
                         }
                        ?>
                        </table>
                        </div>
                        <div class="col-lg-12 text-center"><?php pagination_new($total, $page, $limit, '#', '?page='); ?>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
            <hr>
            <p style="font-weight: bold">
                © 2016 <a href="http://larfess.com" target="_blank">Messias Alfen Febral</a>
            </p>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../assets/js/bootstrap.min.js"></script>

</body>

</html>