<?php
session_start();
if(!isset($_SESSION['id_admin']) || !isset($_SESSION['nama_admin']) || !isset($_SESSION['jabatan'])){
    header("location: ../login.php");
}
include '../../include/koneksi.php';

// Proses Tambah
if($_POST){
	$id_mengajar = $_POST['id_mengajar'];
    $catatan_pengembangan = $_POST['catatan_pengembangan'];
    $catatan = $_POST['catatan'];
    $sikap = $_POST['sikap'];
    $kerajinan = $_POST['kerajinan'];
    $kebersihan_kerapihan = $_POST['kebersihan_kerapihan'];
    $izin = $_POST['izin'];
    $sakit = $_POST['sakit'];
    $tanpa_keterangan = $_POST['tanpa_keterangan'];
    $thn_pel = $_POST['thn_pel'];
    $semester = $_POST['semester'];
    $sql = "INSERT INTO `hasil_raport`(`id_mengajar`, `catatan_pengembangan`, `catatan`, `sikap`, `kerajinan`, `kebersihan_kerapihan`, `izin`, `sakit`, `tanpa_keterangan`, `thn_pel`, `semester`) VALUES ('$id_mengajar','$catatan_pengembangan','$catatan','$sikap','$kerajinan','$kebersihan_kerapihan','$izin','$sakit','$tanpa_keterangan','$thn_pel','$semester')";
    $query = mysql_query($sql);

	if($query){
		// echo '<script>alert("Input Data Berhasil");</script>';
		// header("location: index.php");
        echo '<script>
        alert("Data Berhasil Ditambah");
        window.location.href="index.php";
        </script>';
	}else{
		// echo '<script>alert("Input Data Gagal");</script>';
		// header("location: index.php");
        echo '<script>
        alert("Data Gagal Ditambah");
        window.location.href="index.php";
        </script>';
	}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tambah - Hasil Raport</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../../assets/css/materialize.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../">Pengolah Nilai - Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $_SESSION['nama_admin']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <!-- <li>
                            <a href="../profil"><i class="fa fa-fw fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="../ubah_password"><i class="fa fa-fw fa-gear"></i> Ubah Password</a>
                        </li> -->
                        <li class="divider"></li>
                        <li>
                            <a href="../logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="../"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Login <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="../login_admin">Login Admin</a>
                            </li>
                            <li>
                                <a href="../login_guru">Login Guru</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="../mapel"><i class="fa fa-fw fa-pencil"></i> Mata Pelajaran</a>
                    </li>
                    <li>
                        <a href="../siswa"><i class="fa fa-fw fa-user"></i> Siswa</a>
                    </li>
                    <li>
                        <a href="../guru"><i class="fa fa-fw fa-user"></i> Guru</a>
                    </li>
                    <li>
                        <a href="../kelas"><i class="fa fa-fw fa-building"></i> Kelas</a>
                    </li> 
                    <li>
                        <a href="../mengajar"><i class="fa fa-fw fa-user"></i> Mengajar</a>
                    </li>
                    
                    <li>
                        <a href="../daftar_nilai"><i class="fa fa-fw fa-star"></i> Daftar Nilai</a>
                    </li>
                    <!-- <li>
                        <a href="../ulangan_harian"><i class="fa fa-fw fa-file"></i> Ulangan harian</a>
                    </li>
                    <li>
                        <a href="../tugas"><i class="fa fa-fw fa-file"></i> Tugas/PR</a>
                    </li>
                    <li>
                        <a href="../uts"><i class="fa fa-fw fa-file"></i> UTS</a>
                    </li>
                    
                    <li>
                        <a href="../uas"><i class="fa fa-fw fa-file"></i> UAS/UKK</a>
                    </li> -->
                    <li class="active">
                        <a href="../hasil_raport"><i class="fa fa-fw fa-file"></i> Hasil Raport</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Hasil Raport
                            <small>Tambah Data Hasil Raport</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="../../admin">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> <a href="../../admin/hasil_raport">Hasil Raport</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-plus"></i> Tambah
                            </li>
                        </ol>
                        
                        <!-- Form -->
                        <form action="tambah.php" method="POST" class="box">

						<div class="form-group">
                            <label>Nama Siswa</label>
                            <div class="row">
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_mengajar" autofocus>
                                        <?php 
                                        $sql = "SELECT * FROM `mengajar` INNER JOIN `siswa` ON `siswa`.`no_induk`=`mengajar`.`no_induk` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`mengajar`.`id_kelas` INNER JOIN `guru` ON `guru`.`nip_guru`=`mengajar`.`nip_guru`";
                                        $query = mysql_query($sql);
                                        while ($row = mysql_fetch_array($query)) {
                                            echo '<option value="'.$row['id_mengajar'].'">'.$row['nama_siswa'].' ['.$row['nama_kelas'].'] ['.$row['nama_guru'].']</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tahun Pelajaran</label>
                            <div class="row">
                                <div class="col-lg-2">
                                    <?php 
                                    $wkt_sebelum = date('Y') - 1;
                                    ?>
                                    <input class="form-control" name="thn_pel" value="<?= $wkt_sebelum.'/'.date('Y'); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Semester</label>
                            <div class="row">
                                <div class="col-lg-2">
                                    <select class="form-control" name="semester">
                                        <option value="1">1(SATU)</option>
                                        <option value="2">2(DUA)</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Sikap</label>
                            <div class="row">
                                <div class="col-lg-2">
                                    <select class="form-control" name="sikap">
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Kerajinan</label>
                            <div class="row">
                                <div class="col-lg-2">
                                    <select class="form-control" name="kerajinan">
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Kebersihan/Kerapihan</label>
                            <div class="row">
                                <div class="col-lg-2">
                                    <select class="form-control" name="kebersihan_kerapihan">
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Izin</label>
                            <div class="row">
                                <div class="col-lg-2">
                                    <input class="form-control" name="izin" maxlength="3">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Sakit</label>
                            <div class="row">
                                <div class="col-lg-2">
                                    <input class="form-control" name="sakit" maxlength="3">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tanpa Keterangan</label>
                            <div class="row">
                                <div class="col-lg-2">
                                    <input class="form-control" name="tanpa_keterangan" maxlength="3">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Catatan Pengembangan</label>
                            <div class="row">
                                <div class="col-lg-8">
                                    <textarea class="form-control" name="catatan_pengembangan" rows="7"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Catatan</label>
                            <div class="row">
                                <div class="col-lg-8">
                                    <textarea class="form-control" name="catatan" rows="7"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" name="publish" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>

                        
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <hr>
            <p style="font-weight: bold">
                © 2016 <a href="http://larfess.com" target="_blank">Messias Alfen Febral</a>
            </p>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../assets/js/bootstrap.min.js"></script>

</body>

</html>