<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once '../../plugins/excel/PHPExcel.php';
require_once '../../include/koneksi_mysqli.php';
require_once '../../include/fungsi.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Alfen Febral")
							 ->setLastModifiedBy("Alfen Febral")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


// Add some data
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
       		->setCellValue('A1', 'No')
          ->setCellValue('B1', 'Nama Siswa')
       		->setCellValue('C1', 'Kelas')
       		->setCellValue('D1', 'Mapel')
       		->setCellValue('E1', 'Tulis')
          ->setCellValue('F1', 'Praktek');

// Dapatkan Query
$query="SELECT * FROM `uts` INNER JOIN `mengajar` ON `mengajar`.`id_mengajar`=`uts`.`id_mengajar` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`uts`.`id_daftar_nilai` INNER JOIN `mapel` ON `mapel`.`id_mapel`=`daftar_nilai`.`id_mapel` INNER JOIN `siswa` ON `siswa`.`id_siswa`=`mengajar`.`id_siswa` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`mengajar`.`id_kelas`";
$hasil = mysqli_query($sambung,$query);

// Data
$baris = 2;
$no = 0;
$alp = "E";
while($row=mysqli_fetch_array($hasil)){
$no = $no +1;
$alps=$alp;
++$alps;
$objPHPExcel->setActiveSheetIndex(0)
     ->setCellValue("A$baris", $no)
     ->setCellValue("B$baris", $row['nama_siswa'])
     ->setCellValue("C$baris", $row['nama_kelas'])
     ->setCellValue("D$baris", $row['nama_mapel'])
     ->setCellValue("$alp$baris", $row['tulis'])
     ->setCellValue("$alps$baris", $row['praktek']);
$baris = $baris + 1;
}

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('UTS');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="uts.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
