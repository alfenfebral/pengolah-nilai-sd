<?php
session_start();
if(!isset($_SESSION['id_admin']) || !isset($_SESSION['nama_admin']) || !isset($_SESSION['jabatan'])){
    header("location: ../login.php");
}
include '../../include/koneksi.php';
include '../../include/fungsi.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mata Pelajaran</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../../assets/css/materialize.css" rel="stylesheet">


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../">Pengolah Nilai - Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $_SESSION['nama_admin']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <!-- <li>
                            <a href="../profil"><i class="fa fa-fw fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="../ubah_password"><i class="fa fa-fw fa-gear"></i> Ubah Password</a>
                        </li> -->
                        <li class="divider"></li>
                        <li>
                            <a href="../logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="../"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Login <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="../login_admin">Login Admin</a>
                            </li>
                            <li>
                                <a href="../login_guru">Login Guru</a>
                            </li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="../mapel"><i class="fa fa-fw fa-pencil"></i> Mata Pelajaran</a>
                    </li>
                    <li>
                        <a href="../siswa"><i class="fa fa-fw fa-user"></i> Siswa</a>
                    </li>
                    <li>
                        <a href="../guru"><i class="fa fa-fw fa-user"></i> Guru</a>
                    </li>
                    <li>
                        <a href="../kelas"><i class="fa fa-fw fa-building"></i> Kelas</a>
                    </li> 
                    <li>
                        <a href="../mengajar"><i class="fa fa-fw fa-user"></i> Mengajar</a>
                    </li>
                    
                    <li>
                        <a href="../daftar_nilai"><i class="fa fa-fw fa-star"></i> Daftar Nilai</a>
                    </li>
                    <!-- <li>
                        <a href="../ulangan_harian"><i class="fa fa-fw fa-file"></i> Ulangan harian</a>
                    </li>
                    <li>
                        <a href="../tugas"><i class="fa fa-fw fa-file"></i> Tugas/PR</a>
                    </li>
                    <li>
                        <a href="../uts"><i class="fa fa-fw fa-file"></i> UTS</a>
                    </li>
                    
                    <li>
                        <a href="../uas"><i class="fa fa-fw fa-file"></i> UAS/UKK</a>
                    </li> -->
                    <li>
                        <a href="../hasil_raport"><i class="fa fa-fw fa-file"></i> Hasil Raport</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Mata Pelajaran
                            <small>Data Mata Pelajaran</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="../../admin">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-pencil"></i> Mata Pelajaran
                            </li>
                        </ol>
                        
                        <!-- <form class="form-inline" action="index.php" method="post">
                            <a href="./tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
                            <a href="./print.php" class="btn btn-warning"><i class="fa fa-print"></i> Print</a>
                            <a href="./mapel_xlsx.php" class="btn btn-warning"><i class="fa fa-file-excel-o"></i> Excel</a>
                        </form> -->

                        <?php
                        $limit = 10;
                        if(isset($_GET['page'])){
                            $page = $_GET['page'];
                        }else{
                            $page = 1;
                        }
                        $offset = ($page - 1) * $limit; 
                        $sql = "SELECT * FROM `mapel` ORDER BY `nama_mapel`";
                         $query = mysql_query($sql." LIMIT $offset, $limit");
                         $total = mysql_num_rows(mysql_query($sql));
                        ?>

                        <div class="panel panel-default filterable">
                            <div class="panel-heading">
                                <h3 class="panel-title">Total Data: <?= $total; ?></h3>
                                <div class="pull-right">
                                    <a href="./tambah.php" class="btn btn-success btn-xs btn-filter"><i class="fa fa-plus"></i> Tambah</a> 
                                    <!-- <a href="../print.php" class="btn btn-primary btn-xs btn-filter"><i class="fa fa-print"></i> Print</a> -->
                                </div>
                            </div>
                        <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <th>No</th>
                                <!-- <th>No Induk</th> -->
                                <th>Nama Mapel</th>
                                <th>KKM</th>
                                <th>Jenis Mapel</th>
                                <th>Aksi</th>
                            </thead>
                        <?php 
                        if($total > 0){
                            $no = ($page - 1) * $limit; 
                             while ($row = mysql_fetch_array($query)) {
                                $no++;
                                 echo "<tr>";
                                 echo "<td>".$no."</td>";
                                 echo "<td>".$row['nama_mapel']."</td>";
                                 echo "<td>".$row['kkm']."</td>";
                                 echo "<td>".$row['jenis_mapel']."</td>";
                                 echo '<td><a href="edit.php?id='.$row['id_mapel'].'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Edit</a> <a class="btn btn-sm btn-danger" href="hapus.php?id='.$row['id_mapel'].'" title="Hapus" onclick="return confirm(\'Apakah anda yakin akan menghapus data ini?\')"><i class="fa fa-trash"></i> Delete</a></td>';
                                 echo "</tr>";
                             }
                         }else{
                             echo "<tr><td colspan=\"6\">Data tidak tersedia</td></tr>";
                         }
                        ?>
                        </table>
                        </div>
                        <div class="col-lg-12 text-center"><?php pagination_new($total, $page, $limit, '#', '?page='); ?>
                        </div>
                    </div>
                </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
            <hr>
            <p style="font-weight: bold">
                © 2016 <a href="http://larfess.com" target="_blank">Messias Alfen Febral</a>
            </p>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../assets/js/bootstrap.min.js"></script>

</body>

</html>