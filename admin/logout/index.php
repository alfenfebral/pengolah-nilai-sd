<!DOCTYPE html>
<html>
<head>
	<title>Logout</title>
	<link rel="stylesheet" type="text/css" href="../../assets/css/sweetalert.css">
</head>
<body>


<?php 
session_start();
include '../../include/koneksi.php';
session_destroy();
// header("location: ../login.php");
echo "
 <script type='text/javascript'>
  setTimeout(function () {  
   swal({
    title: 'Berhasil Logout',
    text:  '',
    type: 'success',
    timer: 3000,
    showConfirmButton: true
   });  
  },10); 
  window.setTimeout(function(){ 
   window.location.replace('../login.php');
  } ,3000); 
 </script>"; 
 ?>

	<script src="../../assets/js/jquery.js"></script>
	<script src="../../assets/js/sweetalert.min.js"></script>
</body>
</html>