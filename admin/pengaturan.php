<?php 
include '../include/koneksi.php';

function get_pagin($modul){
    $sql = "SELECT * FROM `pengaturan_pagin` WHERE `modul`='$modul'";
    $query = mysql_query($sql);
    $row = mysql_fetch_assoc($query);
    return $mapel_pagin = $row['perhalaman'];
}

// Proses Tambah
if($_POST){
    $mapel_pagin = $_POST['mapel_pagin'];
    $siswa_pagin = $_POST['siswa_pagin'];
    $guru_pagin = $_POST['guru_pagin'];
    $kelas_pagin = $_POST['kelas_pagin'];
    $mengajar_pagin = $_POST['mengajar_pagin'];
    $daftar_pagin = $_POST['daftar_pagin'];
    $ulangan_pagin = $_POST['ulangan_pagin'];
    $tugas_pagin = $_POST['tugas_pagin'];
    $uts_pagin = $_POST['uts_pagin'];
    $uas_pagin = $_POST['uas_pagin'];
    $hasil_raport_pagin = $_POST['hasil_raport_pagin'];
    $sql1 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$mapel_pagin' WHERE `modul`='mapel'";
    $sql2 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$siswa_pagin' WHERE `modul`='siswa'";
    $sql3 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$guru_pagin' WHERE `modul`='guru'";
    $sql4 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$kelas_pagin' WHERE `modul`='kelas'";
    $sql5 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$mengajar_pagin' WHERE `modul`='mengajar'";
    $sql6 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$daftar_pagin' WHERE `modul`='daftar_nilai'";
    $sql7 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$ulangan_pagin' WHERE `modul`='ulangan'";
    $sql8 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$tugas_pagin' WHERE `modul`='tugas'";
    $sql9 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$uts_pagin' WHERE `modul`='uts'";
    $sql10 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$uas_pagin' WHERE `modul`='uas'";
	$sql11 = "UPDATE `pengaturan_pagin` SET `perhalaman`='$hasil_raport_pagin' WHERE `modul`='hasil_raport'";
    $query1 = mysql_query($sql1);
    $query2 = mysql_query($sql2);
    $query3 = mysql_query($sql3);
    $query4 = mysql_query($sql4);
    $query5 = mysql_query($sql5);
    $query6 = mysql_query($sql6);
    $query7 = mysql_query($sql7);
    $query8 = mysql_query($sql8);
    $query9 = mysql_query($sql9);
    $query10 = mysql_query($sql10);
    $query11 = mysql_query($sql11);

    if($query6){
		header("location: pengaturan.php");
        echo '<script>alert("Input Data Berhasil");</script>';
	}else{
		echo '<script>alert("Input Data Gagal");</script>';
		header("location: index.php");
	}
}else{
    $mapel_pagin = get_pagin('mapel');
    $siswa_pagin = get_pagin('siswa');
    $guru_pagin = get_pagin('guru');
    $kelas_pagin = get_pagin('kelas');
    $mengajar_pagin = get_pagin('mengajar');
    $daftar_pagin = get_pagin('daftar_nilai');
    $ulangan_pagin = get_pagin('ulangan');
    $tugas_pagin = get_pagin('tugas');
    $uts_pagin = get_pagin('uts');
    $uas_pagin = get_pagin('uas');
    $hasil_raport_pagin = get_pagin('hasil_raport');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pengaturan</title>

    <!-- Bootstrap Core CSS -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../assets/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">Pengolah Nilai - Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="./profil"><i class="fa fa-fw fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="./ubah_password"><i class="fa fa-fw fa-gear"></i> Ubah Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="./logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="."><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="./mapel"><i class="fa fa-fw fa-pencil"></i> Mata Pelajaran</a>
                    </li>
                    <li>
                        <a href="./siswa"><i class="fa fa-fw fa-user"></i> Siswa</a>
                    </li>
                    <li>
                        <a href="./guru"><i class="fa fa-fw fa-user"></i> Guru</a>
                    </li>
                    <li>
                        <a href="./kelas"><i class="fa fa-fw fa-building"></i> Kelas</a>
                    </li> 
                    <li>
                        <a href="./mengajar"><i class="fa fa-fw fa-user"></i> Mengajar</a>
                    </li>              
                    <li>
                        <a href="./daftar_nilai"><i class="fa fa-fw fa-star"></i> Daftar Nilai</a>
                    </li>
                    <li>
                        <a href="./ulangan_harian"><i class="fa fa-fw fa-file"></i> Ulangan harian</a>
                    </li>
                    <li>
                        <a href="./tugas"><i class="fa fa-fw fa-file"></i> Tugas/PR</a>
                    </li>
                    <li>
                        <a href="./uts"><i class="fa fa-fw fa-file"></i> UTS</a>
                    </li>
                    
                    <li>
                        <a href="./uas"><i class="fa fa-fw fa-file"></i> UAS/UKK</a>
                    </li>
                    <li>
                        <a href="./hasil_raport"><i class="fa fa-fw fa-file"></i> Hasil Raport</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Pengaturan
                            <small>Pengaturan Pagin</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="../admin">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-gear"></i> Pengaturan
                            </li>
                        </ol>
                        
                        <!-- Form -->
                        <form class="" action="pengaturan.php" method="POST">
                        <div class="form-group">
                            <label>Mata Pelajaran</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="mapel_pagin" value="<?= $mapel_pagin; ?>" autofocus>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Siswa</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="siswa_pagin" value="<?= $siswa_pagin; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Guru</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="guru_pagin" value="<?= $guru_pagin; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Kelas</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="kelas_pagin" value="<?= $kelas_pagin; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Mengajar</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="mengajar_pagin" value="<?= $mengajar_pagin; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Daftar Nilai</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="daftar_pagin" value="<?= $daftar_pagin; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Ulangan Harian</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="ulangan_pagin" value="<?= $ulangan_pagin; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tugas/PR</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="tugas_pagin" value="<?= $tugas_pagin; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>UTS</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="uts_pagin" value="<?= $uts_pagin; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>UAS</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="uas_pagin" value="<?= $uas_pagin; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Hasil Raport</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="hasil_raport_pagin" value="<?= $hasil_raport_pagin; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" name="publish" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>

                        
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <hr>
            <p style="font-weight: bold">
                © 2016 <a href="http://larfess.com" target="_blank">Messias Alfen Febral</a>
            </p>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../assets/js/bootstrap.min.js"></script>

</body>

</html>