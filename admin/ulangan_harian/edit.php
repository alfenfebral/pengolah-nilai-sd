<?php
session_start();
if(!isset($_SESSION['id_admin']) || !isset($_SESSION['nama_admin']) || !isset($_SESSION['jabatan'])){
    header("location: ../login.php");
}
include '../../include/koneksi.php';

// Proses Tambah
if($_POST){
	$id = $_GET['id'];
    $id_daftar_nilai = $_POST['id_daftar_nilai'];
    $id_mengajar = $_POST['id_mengajar'];
    $ulangan_ke = $_POST['ulangan_ke'];
    $nilai = $_POST['nilai'];
    $sql = "UPDATE `ulangan_harian` SET `id_mengajar`='$id_mengajar',`id_daftar_nilai`='$id_daftar_nilai',`ulangan_ke`='$ulangan_ke',`nilai`='$nilai' WHERE `id_ulangan`='$id'";
    $query = mysql_query($sql);

	if($query){
		echo '<script>alert("Input Data Berhasil");</script>';
		header("location: index.php");
	}else{
		echo '<script>alert("Input Data Gagal");</script>';
	}
}

if(isset($_GET['id'])){
	$id = $_GET['id'];
    $sql = "SELECT * FROM `ulangan_harian` WHERE `id_ulangan`='$id'";
    $query = mysql_query($sql);
    if(mysql_num_rows($query) > 0){
        while ($row = mysql_fetch_array($query)) {
            $id_daftar_nilai = $row['id_daftar_nilai'];
            $id_mengajar = $row['id_mengajar'];
            $ulangan_ke = $row['ulangan_ke'];
            $nilai = $row['nilai'];
        }
    }else{
        echo "Not Found";
        die();
    }
}else{
	die();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit - Ulangan Harian</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../assets/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../../assets/css/materialize.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../">Pengolah Nilai - Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $_SESSION['nama_admin']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="../profil"><i class="fa fa-fw fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="../ubah_password"><i class="fa fa-fw fa-gear"></i> Ubah Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="../"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Login <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="../login_admin">Login Admin</a>
                            </li>
                            <li>
                                <a href="../login_guru">Login Guru</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="../mapel"><i class="fa fa-fw fa-pencil"></i> Mata Pelajaran</a>
                    </li>
                    <li>
                        <a href="../siswa"><i class="fa fa-fw fa-user"></i> Siswa</a>
                    </li>
                    <li>
                        <a href="../guru"><i class="fa fa-fw fa-user"></i> Guru</a>
                    </li>
                    <li>
                        <a href="../kelas"><i class="fa fa-fw fa-building"></i> Kelas</a>
                    </li> 
                    <li>
                        <a href="../mengajar"><i class="fa fa-fw fa-user"></i> Mengajar</a>
                    </li>
                    
                    <li>
                        <a href="../daftar_nilai"><i class="fa fa-fw fa-star"></i> Daftar Nilai</a>
                    </li>
                    <li class="active">
                        <a href="../ulangan_harian"><i class="fa fa-fw fa-file"></i> Ulangan harian</a>
                    </li>
                    <li>
                        <a href="../tugas"><i class="fa fa-fw fa-file"></i> Tugas/PR</a>
                    </li>
                    <li>
                        <a href="../uts"><i class="fa fa-fw fa-file"></i> UTS</a>
                    </li>
                    
                    <li>
                        <a href="../uas"><i class="fa fa-fw fa-file"></i> UAS/UKK</a>
                    </li>
                    <li>
                        <a href="../hasil_raport"><i class="fa fa-fw fa-file"></i> Hasil Raport</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Ulangan Harian
                            <small>Edit Data Ulangan Harian</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="../../admin">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> <a href="../../admin/ulangan_harian">Ulangan Harian</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-pencil"></i> Edit
                            </li>
                        </ol>
                        
                        <!-- Form -->
                        <form action="edit.php?id=<?php echo $_GET['id']; ?>" method="POST" class="box">
                        <!-- ID Trans -->
                        <div class="form-group">
                            <label>ID Daftar Nilai</label>
                            <div class="row">
                                <div class="col-lg-8">
                                    <select class="form-control" name="id_daftar_nilai">
                                        <?php 
                                        $sql = "SELECT * FROM `daftar_nilai` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`daftar_nilai`.`id_kelas` INNER JOIN `mapel` ON `mapel`.`id_mapel`=`daftar_nilai`.`id_mapel` INNER JOIN `guru` ON `guru`.`id_guru`=`daftar_nilai`.`id_guru`";
                                        $query = mysql_query($sql);
                                        while ($row = mysql_fetch_array($query)) {
                                        ?>
                                            <option value="<?= $row['id_daftar_nilai']; ?>" <?php if($id_daftar_nilai == $row['id_daftar_nilai']){ echo 'selected'; } ?>><?= $row['id_daftar_nilai'].'.:.'.$row['nama_mapel'].'.:.'.$row['nama_kelas'].'.:.'.$row['nama_guru'].'.:.'.$row['semester'].'.:.'.$row['thn_pel']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- ID Mengajar -->
                        <div class="form-group">
                            <label>ID Mengajar</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <select class="form-control" name="id_mengajar">
                                        <?php
                                        $id_guru = $_SESSION['id_guru'];
                                        $sql = "SELECT * FROM `mengajar` INNER JOIN `siswa` ON `siswa`.`id_siswa`=`mengajar`.`id_siswa` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`mengajar`.`id_kelas` INNER JOIN `guru` ON `guru`.`id_guru`=`mengajar`.`id_guru`";
                                        $query = mysql_query($sql);
                                        while ($row = mysql_fetch_array($query)) {
                                        ?>
                                            <option value="<?= $row['id_mengajar']; ?>" <?php if($id_mengajar == $row['id_mengajar']){ echo 'selected'; } ?>><?= $row['nama_siswa'].' ['.$row['nama_kelas'].'] ['.$row['nama_guru'].']'; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Ke</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="ulangan_ke" placeholder="00" maxlength="2" value="<?= $ulangan_ke; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nilai</label>
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control" name="nilai" placeholder="000" maxlength="3" value="<?= $nilai; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" name="publish" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>

                        
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <hr>
            <p style="font-weight: bold">
                © 2016 <a href="http://larfess.com" target="_blank">Messias Alfen Febral</a>
            </p>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../assets/js/bootstrap.min.js"></script>

</body>

</html>