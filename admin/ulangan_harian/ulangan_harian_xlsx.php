<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once '../../plugins/excel/PHPExcel.php';
require_once '../../include/koneksi_mysqli.php';
require_once '../../include/fungsi.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Alfen Febral")
							 ->setLastModifiedBy("Alfen Febral")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


// Add some data
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(5);
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
       		->setCellValue('A1', 'No')
          ->setCellValue('B1', 'Nama Siswa')
       		->setCellValue('C1', 'Kelas')
       		->setCellValue('D1', 'Mapel')
       		->setCellValue('E1', '1')
          ->setCellValue('F1', '2')
          ->setCellValue('G1', '3')
          ->setCellValue('H1', '4')
          ->setCellValue('I1', '5')
          ->setCellValue('J1', '6')
          ->setCellValue('K1', '7')
       		->setCellValue('L1', '8');

$sql="SELECT * FROM `ulangan_harian` GROUP BY `ulangan_harian`.`id_mengajar`";
$query=mysqli_query($sambung,$sql);
$baris = 2;
$no = 1;
while ($row=mysqli_fetch_array($query)) {
  // $baris_n=2;
  $sql_im = "SELECT * FROM `ulangan_harian` INNER JOIN `mengajar` ON `mengajar`.`id_mengajar`=`ulangan_harian`.`id_mengajar` INNER JOIN `daftar_nilai` ON `daftar_nilai`.`id_daftar_nilai`=`ulangan_harian`.`id_daftar_nilai` INNER JOIN `mapel` ON `mapel`.`id_mapel`=`daftar_nilai`.`id_mapel` INNER JOIN `siswa` ON `siswa`.`id_siswa`=`mengajar`.`id_siswa` INNER JOIN `kelas` ON `kelas`.`id_kelas`=`mengajar`.`id_kelas` WHERE `ulangan_harian`.`id_mengajar`='$row[id_mengajar]' GROUP BY `ulangan_harian`.`id_daftar_nilai`";
  $query_im = mysqli_query($sambung,$sql_im);
  while ($row_im=mysqli_fetch_array($query_im)) {
    $objPHPExcel->setActiveSheetIndex(0)
     ->setCellValue("A$baris", $no++)
     ->setCellValue("B$baris", $row_im['nama_siswa'])
     ->setCellValue("C$baris", $row_im['nama_kelas'])
     ->setCellValue("D$baris", $row_im['nama_mapel']);
    $sql_n = "SELECT * FROM `ulangan_harian` WHERE `ulangan_harian`.`id_daftar_nilai`='$row_im[id_daftar_nilai]' AND `id_mengajar`='$row[id_mengajar]'";
    $query_n = mysqli_query($sambung,$sql_n);
    $alp = "E";
    while ($row_n = mysqli_fetch_array($query_n)) {
      //Output Nilai
      $objPHPExcel->setActiveSheetIndex(0)
     ->setCellValue("$alp$baris", $row_n['nilai']);
     ++$alp;
    }
    // $baris_n = $baris_n+1;
    $baris=$baris+1;
  }
}


// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Ulangan Harian');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="ulangan_harian.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
