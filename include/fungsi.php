<?php 
function pagination_new($total, $page, $limit, $sorot, $link)
{
	echo '<ul class="pagination">';
	$jumData = $total;
	// menentukan jumlah halaman yang muncul berdasarkan jumlah semua data
	$jumPage = ceil($jumData/$limit);
	//menampilkan link << Previous
	if ($page > 1){
	echo'<li><a href="'.$_SERVER['PHP_SELF'].''.$link.''.($page-1).''.$sorot.'" aria-label="Next">&laquo;</a></li>';
	}
	//menampilkan urutan paging
	for($i = 1; $i <= $jumPage; $i++){
	//mengurutkan agar yang tampil i+3 dan i-3
		if ((($i >= $page - 3) && ($i <= $page + 3)) || ($i == 1) || ($i == $jumPage)){
			if($i==$jumPage && $page <= $jumPage-5) echo '<li class="disabled"><span><span aria-hidden="true">...</span></span></li>';
				if ($i == $page) echo ' <li class="active"><a href="'.$sorot.'">'.$i.'</a></li> ';
				else echo ' <li><a href="'.$_SERVER['PHP_SELF'].''.$link.''.$i.''.$sorot.'">'.$i.'</a></li> ';
				if($i==1 && $page >= 6) echo '<li class="disabled"><span><span aria-hidden="true">...</span></span></li>';
		}
	}
	//menampilkan link Next >>
	if ($page < $jumPage){
	echo '<li><a href="'.$_SERVER['PHP_SELF'].''.$link.''.($page+1).''.$sorot.'" aria-label="Next">&raquo;</a></li>';
	}
	echo '</ul>';
}
function jenis_kelamin($data){
	if($data == "l")
		$data = "Laki-Laki";
	else
		$data = "Perempuan";
	return $data;
}

function potong_kata($kata, $batas){
	if(strlen($kata) > $batas){
		$kata = substr($kata, 0, $batas).'...';
	}else{
		$kata = substr($kata, 0, $batas);
	}
	return $kata;
}

function terbilang($x){
    $arr = array("nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    if ($x < 12)
    return " " . $arr[$x];
    elseif ($x < 20)
    return terbilang($x - 10) . " belas";
    elseif ($x < 100)
    return terbilang($x / 10) . " puluh" . terbilang($x % 10);
    elseif ($x < 200)
    return " seratus" . terbilang($x - 100);
    elseif ($x < 1000)
    return terbilang($x / 100) . " ratus" . terbilang($x % 100);
    elseif ($x < 2000)
    return " seribu" . terbilang($x - 1000);
    elseif ($x < 1000000)
    return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    elseif ($x < 1000000000)
    return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
}

function huruf_awal($kata){
	// $pieces = explode(" ", $kata);
	// $pieces[1] = ucwords($pieces[1]); 
	// $hasil = implode(" ", $pieces);
	// return $hasil;
	$replace = ucwords(substr($kata, 1,1));
	return $replace.substr($kata, 2);
}

function romawi($n){
$hasil = "";
$iromawi = array("","I","II","III","IV","V","VI","VII","VIII","IX","X",20=>"XX",30=>"XXX",40=>"XL",50=>"L",
60=>"LX",70=>"LXX",80=>"LXXX",90=>"XC",100=>"C",200=>"CC",300=>"CCC",400=>"CD",500=>"D",600=>"DC",700=>"DCC",
800=>"DCCC",900=>"CM",1000=>"M",2000=>"MM",3000=>"MMM");
if(array_key_exists($n,$iromawi)){
$hasil = $iromawi[$n];
}elseif($n >= 11 && $n <= 99){
$i = $n % 10;
$hasil = $iromawi[$n-$i] . Romawi($n % 10);
}elseif($n >= 101 && $n <= 999){
$i = $n % 100;
$hasil = $iromawi[$n-$i] . Romawi($n % 100);
}else{
$i = $n % 1000;
$hasil = $iromawi[$n-$i] . Romawi($n % 1000);
}
return $hasil;
}

?>